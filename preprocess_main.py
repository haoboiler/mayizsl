#from codes.utils.requirement import install_stopwords
#install_stopwords()
#from codes.preprocess.preprocess_transicd import main
from codes.preprocess.preprocess_kesu import split_data, test_label, cut_all_sentences, build_yuque
from codes.preprocess.preprocess_w2v import pubmed, build_ailab_small, build_ailab_smallest, get_kesu_word_dict, get_kesu_word_dict_old
from codes.preprocess.preprocess_wos import get_data_from_meta, get_data_from_wos_origin, build_glove_w2v, get_all_word_dict

from codes.preprocess.preprocess_topic import get_data_from_meta
import codes.preprocess.preprocess_situation as psit
import codes.preprocess.preprocess_emotion as pemo
import codes.preprocess.preprocess_kesu as pkesu
# from codes.utils.download import download_mimic3
# from codes.config import MIMICDataConfig, TransICDConfig

def preprocess_wos():
    # get_data_from_wos_origin()
    get_data_from_wos_origin()


def preprocess_topic():
    # 初始化为csv
    get_data_from_meta()


if __name__ == '__main__':
    # preprocess_topic()
    # build_yuque()
    # psit.build_label_matrix()
    #pemo.build_label_matrix()
    #pemo.get_data_from_meta()
    pkesu.clean_da_data()
    pkesu.build_easy_label()
