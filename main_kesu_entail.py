# -*- coding: utf-8 -*-
from pypai.job.python_job_builder import PythonJobBuilder
from pypai.job.pytorch_job_builder import PytorchJobBuilder
from pypai.utils.km_conf_utils import KubemakerRole, ResourceSpec
from pypai.conf import KMConf, ExecConf


def main():
    image = 'reg.docker.alibaba-inc.com/aii/aistudio:2880-20220726103345'
    commands = 'source init.sh && python ./train/download_kesu.py --emb_type bert --init False && python -m torch.distributed.launch --nproc_per_node 4 ./train/train_kesu_entail.py'
    master = ExecConf(cpu=20, memory=1024 * 64, gpu_num=4, num=1, gpu_type="v100", disk_m=1024 * 200)
    km_conf = KMConf(
        cluster='stl',  # stl, alipay_et15 （两个公共池子）
        image=image
    )
    gpu_builder = PythonJobBuilder(
        source_root='.',
        command=commands,
        main_file='./train/train_kesu_entail.py',
        master=master,
        km_conf=km_conf,
        k8s_app_name='riskmining',
        k8s_priority='high'
    )

    # gpu_resource = ResourceSpec(core=10, memory_m=1024 * 64, gpu_num=4,  gpu_type="v100", gpu_percent=100, disk_m=1024*200)
    # gpu_master_role = KubemakerRole(count=1, resource_spec=gpu_resource, envs=None)
    # gpu_builder = PytorchJobBuilder(
    #                                 source_root='./',
    #                                 command=commands,
    #                                 image=image,
    #                                 cluster="alipay_et15",
    #                                 pool="kubemaker",
    #                                 master=gpu_master_role
    #                                 )
    
    
    gpu_builder.run()


if __name__ == '__main__':
    main()

