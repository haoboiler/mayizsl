import torch
import pandas as pd
from tqdm import tqdm
from torch.utils.data import Dataset
from codes.utils import save_data, download_file


class AG_Dataset(Dataset):
    """
    dataset类
    """
    def __init__(self, dataset_config, data_type, emb_layer, logger, label=False, init=True, debug=False, local=True):
        '''
        init: bool  if True, build dataset by embedding layer, otherwise, read it
        '''
        self.local = local  # local or mayi server
        self.dataset_config = dataset_config  # config

        self.emb_layer = emb_layer  # embedding layer
        # EDA
        from model.new_model.augmentation.eda import eda
        self.eda = eda  # eda function
        # download unlabeled data
        df = pd.read_csv(
            dataset_config.local_path[data_type],
            sep=",",
            error_bad_lines=False,
            header=None,
            names=['label', 'title', 'text'],
            skiprows=None,
            quoting=0,
            keep_default_na=False,
            encoding="utf-8",
        )
        self.text = list(df['text'])
        self.parent_adj = None
        self.child_adj = None
        self.label = list(df['label'])

        # start data augmentation
        # if init:
        #     self.text_da = []
        #     for idx in tqdm(range(len(self.text))):
        #         text = self.text[idx]
        #         # abletion study: da and contrastive loss
        #         da_text = self.data_augmentation(text)
        #         self.text_da.append(da_text)
        #     new_df = pd.DataFrame()
        #     new_df['text'] = self.text
        #     new_df['da_text'] = self.text_da
        #     new_df.to_csv(dataset_config.local_path[data_type])
        #     save_data(dataset_config.oss_path[data_type], dataset_config.local_path[data_type])

        # df = pd.read_csv(dataset_config.local_path[data_type])
        # # debug: only use 2 batch size data to fast debug
        # self.text = list(df['text'])
        # self.text_da = list(df['da_text'])

    def data_augmentation(self, text):
        eda_text = self.eda(text, num_aug=1)
        return eda_text

    def __getitem__(self, index: int):
        return self.text[index], self.label[index], self.text[index]

    def __len__(self):
        return len(self.text)


class AG_Generated_Dataset(Dataset):
    '''
    generated class and result
    '''
    def __init__(self, class_num, dataset_config):
        '''
        init: bool  if True, build dataset by embedding layer, otherwise, read it
        '''
        self.class_num = class_num
        label_csv = pd.read_csv(dataset_config.label_local_path, header=None, names=['index', 'label'])
        self.y_text = list(label_csv['label'])
        print('y_label length', len(self.y_text))
        self.x = []
        self.y = []
        for i in range(class_num):
            self.x.append(torch.tensor(i))
            self.y.append(torch.tensor(i))

    def __getitem__(self, index: int):
        return self.x[index], self.y[index], self.y_text[index]

    def __len__(self):
        return len(self.x)
