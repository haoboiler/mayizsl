import os


class AGDataConfig():
    def __init__(self, easy_label=False):
        self.task_type = 'Single_Label'

        # dataset download and path config
        if easy_label:
            self.dataset_oss_data_path = 'cunyin/ag_news'
            self.dataset_oss_emb_path = 'cunyin/ag_news/emb_output'
        else:
            self.dataset_oss_data_path = 'cunyin/ag_news'
            self.dataset_oss_emb_path = 'cunyin/ag_news/emb_output'
        self.dataset_local_data_path = './data/'
        # unlabeled data
        self.unlabel_oss_path = os.path.join(self.dataset_oss_data_path, 'unlabeled_data.csv')
        self.unlabel_local_path = os.path.join(self.dataset_local_data_path, 'unlabeled_data.csv')
        # label description
        self.label_oss_path = os.path.join(self.dataset_oss_data_path, 'labels.csv')
        self.label_local_path = os.path.join(self.dataset_local_data_path, 'labels.csv')
        # labeled dataset
        self.oss_path = {}
        self.local_path = {}
        for data_type in ['train', 'dev', 'test']:
            self.oss_path[data_type] = os.path.join(self.dataset_oss_data_path, '%s.csv' % data_type)
            self.local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s.csv' % data_type)

    def embed_config(self):
        '''
        embedding config
        '''
        self.save_local_path = {}
        self.save_oss_path = {}
        self.save_da_local_path = {}
        self.save_da_oss_path = {}
        self.save_label_local_path = os.path.join(self.dataset_local_data_path, 'label_%s.pt' % self.emb_type)
        self.save_label_oss_path = os.path.join(self.dataset_oss_emb_path, 'label_%s.pt' % self.emb_type)
        self.save_easy_label_local_path = os.path.join(self.dataset_local_data_path, 'easy_label_%s.pt' % self.emb_type)
        self.save_easy_label_oss_path = os.path.join(self.dataset_oss_emb_path, 'easy_label_%s.pt' % self.emb_type)
        for data_type in ['train', 'dev', 'test']:
            self.save_local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s_%s.pt' %
                                                           (data_type, self.emb_type))
            self.save_da_local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s_%s_da.pt' %
                                                              (data_type, self.emb_type))
            self.save_oss_path[data_type] = os.path.join(self.dataset_oss_emb_path, '%s_%s.pt' %
                                                         (data_type, self.emb_type))
            self.save_da_oss_path[data_type] = os.path.join(self.dataset_oss_emb_path, '%s_%s_da.pt' %
                                                            (data_type, self.emb_type))

    def da(self, da_type='eda'):
        self.da_type = da_type


class AGDataConfig_W2V(AGDataConfig):
    def __init__(self, emb_type='bert', easy_label=False):
        super().__init__(easy_label=easy_label)
        self.task_type = 'Single_Label'

        # w2v download path
        self.w2v_model_path = './pretrained_model/w2v_embedding.pt'
        self.w2v_vocab_path = './pretrained_model/tokens.npy'
        self.w2v_oss_path = 'cunyin/ag_news'
        self.w2v_local_path = './pretrained_model'
        self.w2v_file_list = ['tokens.npy']

        # embedding setting
        self.embed_config()
        # augmentation setting
        self.da()

    def embed_config(self):
        '''
        embedding config
        '''
        print('use w2v type')
        self.emb_type = 'w2v'
        super().embed_config()
        self.padding = True
        self.max_len = 200
        self.emb_size = 200




AGDataConfigDICT = {
    'w2v': AGDataConfig_W2V,
}
