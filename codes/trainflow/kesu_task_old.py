import os
import time
from codes.utils.requirement import install_synonyms
from codes.trainflow.taskflow import TaskFlow
from codes.utils import download_file, Logger
from codes.kesu.dataloader import (
    KESU_Dataset,
    KESU_Idx_Dataset,
    KESU_Generated_Dataset,
    KESU_Unlabeled_Dataset,
    KESU_Idx_Unlabeled_Dataset,
    KESU_Dataset_EASY,
    KESU_Idx_Dataset_EASY,
    KESU_Idx_Label_dataset
    )
from codes.kesu.emb import EmbeddingLayer
# import model config
from codes.kesu.model_config import ModelConfig
from codes.kesu.dataset_config import KESUDataConfigDICT, KESUDataConfig_Local
from model import ModelTrainer, ModelTrainerParallel
from model.cv_trainer import TaskTrainer, TaskTrainerBERT

class KESUFlow(TaskFlow):
    def __init__(self, model_dict, dataset_dict, emb_type='w2v', parallel=False, local=True, model='New', debug=False,
                 init=True, download=True, last_label=True, pre_idx=True, decoder_gpt=False):
        '''
        model : ['New', 'TransICD']
        local: bool local train or aistudio train
        pre_idx: calculate idx before start train
        '''
        super().__init__()
        self.parallel = parallel
        self.emb_type = emb_type
        self.init = init
        self.local = local
        self.download = download
        self.model = model
        self.debug = debug
        self.last_label = last_label
        self.decoder_gpt = decoder_gpt
        self.pre_idx = pre_idx
        # dataset config
        if self.emb_type == 'bert':
            if self.local:
                self.data_config = KESUDataConfig_Local(emb_type=self.emb_type, **dataset_dict)
            else:
                self.data_config = KESUDataConfigDICT['bert_t'](emb_type=self.emb_type, **dataset_dict)
                self.data_bert_p_config = KESUDataConfigDICT['bert_p'](emb_type=self.emb_type, **dataset_dict)
        elif self.emb_type == 'w2v':
            if self.local:
                self.data_config = KESUDataConfig_Local(emb_type=self.emb_type, **dataset_dict)
            else:
                self.data_config = KESUDataConfigDICT['w2v'](emb_type=self.emb_type, **dataset_dict)

        # choose the model config
        self.model_config = ModelConfig[model](emb_size=self.data_config.emb_size,
                                               seq_len=self.data_config.max_len,
                                               **model_dict)
        self.model_config.task_type = self.data_config.task_type
        # push gpt config to model config
        if self.decoder_gpt == True:
            self.gpt_config = KESUDataConfigDICT['gpt2'](emb_type=self.emb_type, **dataset_dict)
            self.model_config.gpt_config = self.gpt_config  # type: ignore
        self.logger = Logger().logger
        print(self.parallel, self.emb_type, self.model)

    def requirement_install(self):
        self.logger.info('start requirement')
        if self.download:
            self.logger.info('start requirement')
            install_synonyms(local=self.local)  # for eda
            
        if self.download is True and self.local is False:
            if self.emb_type == 'bert':
                # download bert_t
                if os.path.exists(self.data_config.bert_path) is False:  # type: ignore
                    os.mkdir(self.data_config.bert_path)  # type: ignore
                for file in self.data_config.bert_file_list:  # type: ignore
                    download_file(os.path.join(self.data_config.bert_oss_path, file),  # type: ignore
                                os.path.join(self.data_config.bert_path, file))  # type: ignore
                # download bert_p
                if os.path.exists(self.data_bert_p_config.bert_path) is False:
                    os.mkdir(self.data_bert_p_config.bert_path)
                for file in self.data_bert_p_config.bert_file_list:
                    download_file(os.path.join(self.data_bert_p_config.bert_oss_path, file),
                                os.path.join(self.data_bert_p_config.bert_path, file))
            elif self.emb_type == 'w2v':
                # download w2v file
                for file_name in self.data_config.w2v_file_list:  # type: ignore
                    download_file(os.path.join(self.data_config.w2v_oss_path, file_name),  # type: ignore
                                  os.path.join(self.data_config.w2v_local_path, file_name))  # type: ignore
            # download label data from oss
            self.logger.info('start download data')
            download_file(self.data_config.label_oss_path, self.data_config.label_local_path)
            # download total data from oss
            for data_type in ['total']:
                download_file(self.data_config.oss_path[data_type], self.data_config.local_path[data_type])
            # download unlabeled data from oss
            download_file(self.data_config.unlabel_oss_path, self.data_config.unlabel_local_path)
        else:
            pass
        time.sleep(60)

    def preprocess_data(self):
        # download_bert
        init = self.init
        debug = self.debug
        local = self.local
        # initial embedding layer
        if self.emb_type == 'w2v':
            self.emb_layer = EmbeddingLayer(self.data_config)  # embedding type
        elif self.emb_type == 'bert':
            self.emb_layer = EmbeddingLayer(self.data_config)  # embedding type
            self.emb_layer_p = EmbeddingLayer(self.data_bert_p_config)  # embedding type
        else:
            raise TypeError
        
        # load dataset
        self.dataloader_list = []
        if self.last_label:
            if self.pre_idx:
                self.total_dataset = KESU_Idx_Dataset_EASY(self.data_config, 'total', logger=self.logger, label=True,
                                                           emb_layer=self.emb_layer, init=init, local=local, debug=debug)
            else:
                self.total_dataset = KESU_Dataset_EASY(self.data_config, 'total', logger=self.logger, label=True,
                                                       emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        else:
            # load dataset
            if self.pre_idx:
                self.total_dataset = KESU_Idx_Dataset(self.data_config, 'total', logger=self.logger, label=True,
                                                      emb_layer=self.emb_layer, init=init, local=local, debug=debug)
            else:
                self.total_dataset = KESU_Dataset(self.data_config, 'total', logger=self.logger, label=True,
                                                emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        self.dataloader_list.append(self.total_dataset)
        
        # load generate dataset
        self.logger.info('end unlabeled dataset build')
        self.label_mat = self.total_dataset.label_mat
        self.model_config.class_num = self.label_mat.shape[0]  # type: ignore
        self.generate_dataset = KESU_Generated_Dataset(class_num=self.model_config.class_num,
                                                       dataset_config=self.data_config)
        # load unlabel dataset
        self.logger.info('end total dataset build')
        if self.pre_idx:
            self.unlabel_dataset = KESU_Idx_Unlabeled_Dataset(dataset_config=self.data_config, init=init,
                                                              emb_layer=self.emb_layer, debug=debug, local=local)
            # self.unlabel_dataset = KESU_Unlabeled_Dataset(dataset_config=self.data_config, init=init,
            #                                               emb_layer=self.emb_layer, debug=debug, local=local)           
        else:
            self.unlabel_dataset = KESU_Unlabeled_Dataset(dataset_config=self.data_config, init=init,
                                                          emb_layer=self.emb_layer, debug=debug, local=local)
        self.dataloader_list.append(self.unlabel_dataset)

        # label description dataset
        label_dataset = KESU_Idx_Label_dataset(self.data_config, data_type='eval', logger=self.logger, label=True,
                                            emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        self.dataloader_list.append(label_dataset)
        
    def train(self):
        self.logger.info('start training, with label num %d' % self.model_config.class_num)
        #dataloader_list = [self.train_dataset, self.test_dataset, self.dev_dataset, self.unlabel_dataset]
        dataloader_list = self.dataloader_list
        # build model trainer
        if self.parallel:
            model_trainer = ModelTrainerParallel[self.emb_type][self.model]
        else:
            model_trainer = ModelTrainer[self.emb_type][self.model]
        # build task trainer for cross validation training
        if self.emb_type == 'w2v':
            trainer = TaskTrainer(model_config=self.model_config,
                                logger=self.logger,
                                dataloader_list=dataloader_list,
                                model_trainer=model_trainer,
                                label_mat=self.label_mat,
                                generated_dataset=self.generate_dataset,
                                emb_layer=self.emb_layer,
                                local=self.local)
        elif self.emb_type == 'bert':
            trainer = TaskTrainerBERT(model_config=self.model_config,
                                    logger=self.logger,
                                    dataloader_list=dataloader_list,
                                    model_trainer=model_trainer,
                                    label_mat=self.label_mat,
                                    generated_dataset=self.generate_dataset,
                                    emb_layer_t=self.emb_layer,
                                    emb_layer_p=self.emb_layer_p,
                                    local=self.local)
        else:
            raise TypeError
        trainer.train()
        # trainer.test()

    def test(self):
        pass
