from codes.trainflow.mimic_task import MIMICFlow
from codes.trainflow.kesu_task import KESUFlow
from codes.trainflow.agnews_task import AGFlow
from codes.trainflow.wos_task import WOSFlow
from codes.trainflow.topic_task import TopicFlow
from codes.trainflow.situation_task import SituationFlow
from codes.trainflow.emotion_task import EmotionFlow