import os
from codes.utils.requirement import install_wordnet
from codes.trainflow.taskflow import TaskFlow
from codes.mimic.dataset_config import MIMICDataConfig, MIMICDataConfig_Local
from codes.mimic.model_config import ModelConfig
from codes.utils import download_file, Logger
from codes.mimic.dataloader import MIMIC_Dataset, MIMIC_Generated_Dataset, MIMIC_Unlabeled_Dataset
from codes.embed import EmbeddingLayer
from model import ModelTrainer, ModelTrainerParallel


class MIMICFlow(TaskFlow):
    def __init__(self, model_dict, dataset_dict, emb_type='w2v', parallel=False, local=True, model='New', debug=False,
                 init=True, download=True):
        '''
        model : ['New', 'TransICD']
        local: bool local train or aistudio train
        '''
        super().__init__()
        self.parallel = parallel
        self.emb_type = emb_type
        self.init = init
        self.local = local
        self.download = download
        self.model = model
        self.debug = debug
        # dataset config
        if self.local:
            self.data_config = MIMICDataConfig_Local(emb_type=self.emb_type, **dataset_dict)
        else:
            self.data_config = MIMICDataConfig(emb_type=self.emb_type, **dataset_dict)
        # choose the model config
        self.model_config = ModelConfig[model](emb_size=self.data_config.emb_size,
                                               seq_len=self.data_config.max_len, **model_dict)
        self.model_config.task_type = self.data_config.task_type
        self.logger = Logger().logger
        print(self.parallel, self.emb_type, self.model)

    def requirement_install(self):
        self.logger.info('start requirement')
        install_wordnet(local=self.local)  # for eda

    def preprocess_data(self):
        # download_bert
        init = self.init
        debug = self.debug
        local = self.local

        if self.download is True and self.local is False:
            if self.emb_type == 'bert':
                if os.path.exists('./pretrained_model/bert') is False:
                    os.mkdir('./pretrained_model/bert')
                for file in ['config.json', 'pytorch_model.bin', 'vocab.txt',
                             'special_tokens_map.json', 'tokenizer_config.json']:
                    download_file(os.path.join(self.data_config.bert_oss_path, file),
                                  os.path.join(self.data_config.bert_path, file))
            elif self.emb_type == 'w2v':
                for file_name in self.data_config.w2v_file_list:
                    download_file(os.path.join(self.data_config.w2v_oss_path, file_name),
                                  os.path.join(self.data_config.w2v_local_path, file_name))
            # download splited mimic3 data and label from oss
            self.logger.info('start download data')
            download_file(self.data_config.label_oss_path, self.data_config.label_local_path)
            for data_type in ['train', 'dev', 'test']:
                download_file(self.data_config.oss_path[data_type], self.data_config.local_path[data_type])
        else:
            pass

        # use model
        self.emb_layer = EmbeddingLayer(self.data_config)  # embedding type

        # load dataset
        self.logger.info('start build dataset, type is %s' % self.emb_type)
        self.train_dataset = MIMIC_Dataset(self.data_config, 'train', logger=self.logger,
                                           emb_layer=self.emb_layer, label=True, init=init, local=local, debug=debug)
        self.logger.info('end train dataset build')
        self.dev_dataset = MIMIC_Dataset(self.data_config, 'dev', logger=self.logger,
                                         emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        self.logger.info('end dev dataset build')
        self.test_dataset = MIMIC_Dataset(self.data_config, 'test', logger=self.logger,
                                          emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        self.logger.info('end test dataset build')
        self.unlabel_dataset = MIMIC_Unlabeled_Dataset(dataset_config=self.data_config,
                                                       emb_layer=self.emb_layer, debug=debug, local=local)
        self.logger.info('end unlabeled dataset build')
        self.label_mat = self.train_dataset.label_mat
        self.generate_dataset = MIMIC_Generated_Dataset(self.label_mat.shape[0],
                                                        dataset_config=self.data_config)

    def train(self):
        self.model_config.class_num = self.label_mat.shape[0]
        self.logger.info('start training, with label num %d' % self.model_config.class_num)
        dataloader_list = [self.train_dataset, self.test_dataset, self.dev_dataset, self.unlabel_dataset]
        if self.parallel:
            trainer = ModelTrainerParallel[self.model](self.model_config, self.logger, dataloader_list, self.label_mat,
                                                       generated_dataset=self.generate_dataset,
                                                       emb_layer=self.emb_layer, local=self.local)
        else:
            trainer = ModelTrainer[self.model](self.model_config, self.logger, dataloader_list,
                                               self.label_mat, generated_dataset=self.generate_dataset,
                                               emb_layer=self.emb_layer, local=self.local)

        trainer.train()
        trainer.test()

    def test(self):
        pass
