import os
import time
from codes.utils.requirement import install_synonyms
from codes.trainflow.taskflow import TaskFlow
from codes.utils import download_file, Logger
from codes.kesu.dataloader import (
    KESU_Dataset,
    KESU_Idx_Dataset,
    KESU_Generated_Dataset,
    KESU_Unlabeled_Dataset,
    KESU_Dataset_EASY,
    KESU_Idx_Dataset_EASY
    )
from codes.kesu.emb import EmbeddingLayer
# import model config
from codes.kesu.model_config import ModelConfig
from codes.kesu.dataset_config import KESUDataConfigDICT, KESUDataConfig_Local
from model import ModelTrainer, ModelTrainerParallel
from model.cv_trainer import TaskTrainerBERT
from torch.multiprocessing import Process


class KESUBERTFlow(TaskFlow):
    def __init__(self, model_dict, dataset_dict, emb_type='w2v', parallel=False, local=True, model='New', debug=False,
                 init=True, download=True, last_label=True, pre_idx=True):
        '''
        model : ['New', 'TransICD']
        local: bool local train or aistudio train
        pre_idx: calculate idx before start train
        '''
        super().__init__()
        self.parallel = parallel
        self.emb_type = emb_type
        self.init = init
        self.local = local
        self.download = download
        self.model = model
        self.debug = debug
        self.last_label = last_label
        self.pre_idx = pre_idx
        # dataset config
        if self.local:
            self.data_config = KESUDataConfig_Local(emb_type=self.emb_type, **dataset_dict)
        else:
            self.data_bert_t_config = KESUDataConfigDICT['bert_t'](emb_type=self.emb_type, **dataset_dict)
            self.data_bert_p_config = KESUDataConfigDICT['bert_p'](emb_type=self.emb_type, **dataset_dict)

        # choose the model config
        self.model_config = ModelConfig[model](emb_size=self.data_bert_t_config.emb_size,
                                               seq_len=self.data_bert_t_config.max_len, **model_dict)
        self.model_config.task_type = self.data_bert_t_config.task_type
        self.logger = Logger().logger
        print(self.parallel, self.emb_type, self.model)

    def requirement_install(self):
        self.logger.info('start requirement')
        install_synonyms(local=self.local)  # for eda

    def preprocess_data(self):
        # download_bert
        init = self.init
        debug = self.debug
        local = self.local
        if self.download is True and self.local is False:
            # download bert_t
            if os.path.exists(self.data_bert_t_config.bert_path) is False:
                os.mkdir(self.data_bert_t_config.bert_path)
            for file in self.data_bert_t_config.bert_file_list:
                download_file(os.path.join(self.data_bert_t_config.bert_oss_path, file),
                              os.path.join(self.data_bert_t_config.bert_path, file))
            self.logger.info('start download data')
            download_file(self.data_bert_t_config.label_oss_path, self.data_bert_t_config.label_local_path)
            # download bert_p
            if os.path.exists(self.data_bert_p_config.bert_path) is False:
                os.mkdir(self.data_bert_p_config.bert_path)
            for file in self.data_bert_p_config.bert_file_list:
                download_file(os.path.join(self.data_bert_p_config.bert_oss_path, file),
                              os.path.join(self.data_bert_p_config.bert_path, file))
            # download splited kesu data and label from oss
            for data_type in ['total']:
                download_file(self.data_bert_t_config.oss_path[data_type], self.data_bert_t_config.local_path[data_type])
            # unlabel data
            download_file(self.data_bert_t_config.unlabel_oss_path, self.data_bert_t_config.unlabel_local_path)
        else:
            pass
        time.sleep(60)
        # use model
        self.emb_layer_t = EmbeddingLayer(self.data_bert_t_config)  # embedding type
        self.emb_layer_p = EmbeddingLayer(self.data_bert_p_config)  # embedding type

        if self.last_label:
            if self.pre_idx:
                self.total_dataset = KESU_Idx_Dataset_EASY(self.data_bert_t_config, 'total', logger=self.logger, label=True,
                                                           emb_layer=self.emb_layer_t, init=init, local=local, debug=debug)
            else:
                self.total_dataset = KESU_Dataset_EASY(self.data_bert_t_config, 'total', logger=self.logger, label=True,
                                                       emb_layer=self.emb_layer_t, init=init, local=local, debug=debug)
        else:
            # load dataset
            if self.pre_idx:
                self.total_dataset = KESU_Idx_Dataset(self.data_bert_t_config, 'total', logger=self.logger, label=True,
                                                      emb_layer=self.emb_layer_t, init=init, local=local, debug=debug)
            else:
                self.total_dataset = KESU_Dataset(self.data_bert_t_config, 'total', logger=self.logger, label=True,
                                                emb_layer=self.emb_layer_t, init=init, local=local, debug=debug)
        self.logger.info('end unlabeled dataset build')
        self.label_mat = self.total_dataset.label_mat
        self.generate_dataset = KESU_Generated_Dataset(class_num=self.label_mat.shape[0],
                                                       dataset_config=self.data_bert_t_config)

        self.logger.info('end total dataset build')
        self.unlabel_dataset = KESU_Unlabeled_Dataset(dataset_config=self.data_bert_t_config, init=init,
                                                      emb_layer=self.emb_layer_t, debug=debug, local=local)

    def train(self):
        self.model_config.class_num = self.label_mat.shape[0]
        self.logger.info('start training, with label num %d' % self.model_config.class_num)
        dataloader_list = [self.total_dataset, self.unlabel_dataset]
        if self.parallel:
            model_trainer = ModelTrainerParallel[self.emb_type][self.model]
        else:
            model_trainer = ModelTrainer[self.emb_type][self.model]

        trainer = TaskTrainerBERT(model_config=self.model_config,
                                  logger=self.logger,
                                  dataloader_list=dataloader_list,
                                  model_trainer=model_trainer,
                                  label_mat=self.label_mat,
                                  generated_dataset=self.generate_dataset,
                                  emb_layer_t=self.emb_layer_t,
                                  emb_layer_p=self.emb_layer_p,
                                  local=self.local)
        trainer.train()
        # trainer.test()

    def test(self):
        pass
