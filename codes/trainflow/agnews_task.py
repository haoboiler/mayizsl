import os
from codes.utils.requirement import install_synonyms
from codes.trainflow.taskflow import TaskFlow
from codes.utils import download_file, Logger
from codes.ag_news.dataloader import AG_Dataset, AG_Generated_Dataset
from codes.ag_news.emb import EmbeddingLayer
# import model config
from codes.ag_news.model_config import ModelConfig
from codes.ag_news.dataset_config import AGDataConfigDICT
from model import ModelTrainer, ModelTrainerParallel
from model.cv_trainer import TaskTrainer


class AGFlow(TaskFlow):
    def __init__(self, model_dict, dataset_dict, emb_type='w2v', parallel=False, local=True, model='New', debug=False,
                 init=True, download=True, last_label=True):
        '''
        model : ['New', 'TransICD']
        local: bool local train or aistudio train
        '''
        super().__init__()
        self.parallel = parallel
        self.emb_type = emb_type
        self.init = init
        self.local = local
        self.download = download
        self.model = model
        self.debug = debug
        self.last_label = last_label
        # dataset config
        if self.local:
            raise NotImplementedError
        else:
            self.data_config = AGDataConfigDICT['w2v'](emb_type=self.emb_type, **dataset_dict)
        # choose the model config
        self.model_config = ModelConfig[model](emb_size=self.data_config.emb_size,
                                               seq_len=self.data_config.max_len, **model_dict)
        self.model_config.task_type = self.data_config.task_type
        self.logger = Logger().logger
        print(self.parallel, self.emb_type, self.model)

    def requirement_install(self):
        self.logger.info('start requirement')
        install_synonyms(local=self.local)  # for eda

    def preprocess_data(self):
        # download_bert
        init = self.init
        debug = self.debug
        local = self.local
        if self.download is True and self.local is False:
            if self.emb_type == 'bert':
                raise NotImplementedError
            elif self.emb_type == 'w2v':
                for file_name in self.data_config.w2v_file_list:
                    download_file(os.path.join(self.data_config.w2v_oss_path, file_name),
                                  os.path.join(self.data_config.w2v_local_path, file_name))
            # download splited mimic3 data and label from oss
            # self.logger.info('start download data')
            download_file(self.data_config.label_oss_path, self.data_config.label_local_path)
            for data_type in ['train', 'test', 'dev']:
                download_file(self.data_config.oss_path[data_type], self.data_config.local_path[data_type])
        else:
            pass

        # use model
        self.emb_layer = EmbeddingLayer(self.data_config)  # embedding type

        # load dataset

        self.total_dataset = AG_Dataset(self.data_config, 'train', logger=self.logger, label=True,
                                        emb_layer=self.emb_layer, init=init, local=local, debug=debug)
        self.logger.info('end unlabeled dataset build')
        self.generate_dataset = AG_Generated_Dataset(class_num=4,
                                                       dataset_config=self.data_config)
        
    def train(self):
        self.model_config.class_num = 4
        self.logger.info('start training, with label num %d' % self.model_config.class_num)
        #dataloader_list = [self.train_dataset, self.test_dataset, self.dev_dataset, self.unlabel_dataset]
        dataloader_list = [self.total_dataset, self.total_dataset]
        if self.parallel:
            model_trainer = ModelTrainerParallel[self.emb_type][self.model]

            # trainer = ModelTrainerParallel[self.model](self.model_config, self.logger, dataloader_list,
            #                                           self.label_mat, generated_dataset=self.generate_dataset,
            #                                           emb_layer=self.emb_layer, local=self.local)
        else:
            model_trainer = ModelTrainer[self.emb_type][self.model]
            # trainer = ModelTrainer[self.model](self.model_config, self.logger, dataloader_list,
            #                                    self.label_mat, generated_dataset=self.generate_dataset,
            #                                    emb_layer=self.emb_layer, local=self.local)
        trainer = TaskTrainer(model_config=self.model_config,
                              logger=self.logger,
                              dataloader_list=dataloader_list,
                              model_trainer=model_trainer,
                              label_mat=None,
                              generated_dataset=self.generate_dataset,
                              emb_layer=self.emb_layer,
                              local=self.local)
        trainer.train()
        # trainer.test()

    def test(self):
        pass
