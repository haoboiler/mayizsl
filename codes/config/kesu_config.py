import os


class KESUDataConfig():
    def __init__(self, emb_type='bert'):
        self.task_type = 'Single_Label'
        # embedding config
        self.bert_path = './pretrained_model/bert'  # bert save path
        self.bert_oss_path = 'cunyin/pretrained_model/ernie_yuque'  # remote oss bert path
        self.bert_file_list = ['/config.json', '/pytorch_model.bin', '/vocab.txt', '/tokenizer_config.json', 'special_tokens_map.json']

        # w2v download path
        self.w2v_model_path = './pretrained_model/w2v_embedding.pt'
        self.w2v_vocab_path = './pretrained_model/vocab.npy'
        self.w2v_oss_path = 'cunyin/pretrained_model/w2v_yuque'
        self.w2v_local_path = './pretrained_model'
        self.w2v_file_list = ['w2v_embedding.pt', 'vocab.npy']
        self.unk = 2479029
        self.pad = 56176
        self.eos = 68531
        self.bos = 277031

        # dataset download and path config
        self.dataset_oss_data_path = 'cunyin/kesu'
        self.dataset_oss_emb_path = 'cunyin/kesu/emb_output'
        self.dataset_local_data_path = './data/'
        self.label_oss_path = os.path.join(self.dataset_oss_data_path, 'clean_label.csv')
        self.label_local_path = os.path.join(self.dataset_local_data_path, 'clean_label.csv')
        self.oss_path = {}
        self.local_path = {}
        for data_type in ['train', 'dev', 'test']:
            self.oss_path[data_type] = os.path.join(self.dataset_oss_data_path, '%s.csv' % data_type)
            self.local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s.csv' % data_type)
        # embedding setting
        self.embed_config(emb_type=emb_type)
        # augmentation setting
        self.da()

    def embed_config(self, emb_type='bert'):
        '''
        embedding config
        '''
        if emb_type == 'bert':
            print('use bert type')
            self.emb_type = 'bert'
            self.get_cls = False
            self.padding = True
            self.max_len = 200
            self.emb_size = 768
        elif emb_type == 'w2v':
            print('use w2v type')
            self.emb_type = 'w2v'
            self.padding = True
            self.max_len = 200
            self.emb_size = 200
        else:
            raise KeyError
        self.save_local_path = {}
        self.save_oss_path = {}
        self.save_da_local_path = {}
        self.save_da_oss_path = {}
        self.save_label_local_path = os.path.join(self.dataset_local_data_path, 'label_%s.pt' % emb_type)
        self.save_label_oss_path = os.path.join(self.dataset_oss_emb_path, 'label_%s.pt' % emb_type)
        for data_type in ['train', 'dev', 'test']:
            self.save_local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s_%s.pt' % (data_type, self.emb_type))
            self.save_da_local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s_%s_da.pt' % (data_type, self.emb_type))
            self.save_oss_path[data_type] = os.path.join(self.dataset_oss_emb_path, '%s_%s.pt' %  (data_type, self.emb_type))
            self.save_da_oss_path[data_type] = os.path.join(self.dataset_oss_emb_path, '%s_%s_da.pt' %  (data_type, self.emb_type))

    def da(self, da_type='eda'):
        self.da_type = da_type


class KESUDataConfig_Local(KESUDataConfig):
    def __init__(self, emb_type='bert'):
        super().__init__(emb_type=emb_type)
        # dataset download and path config
        self.dataset_local_data_path = '/data/'
        self.label_local_path = os.path.join(self.mimic3_local_data_path, 'clean_label.csv')
        self.local_path = {}
        for data_type in ['train', 'dev', 'test']:
            self.local_path[data_type] = os.path.join(self.dataset_local_data_path, '%s.csv' % data_type)
        # embedding setting
        self.embed_config(emb_type=emb_type)
        # augmentation setting
        self.da()

    def embed_config(self, emb_type='bert'):
        '''
        embedding config
        '''
        if emb_type == 'bert':
            print('use bert type')
            self.emb_type = 'bert'
            self.get_cls = False
            self.padding = True
            self.max_len = 200
            self.emb_size = 768
        elif emb_type == 'w2v':
            print('use w2v type')
            self.emb_type = 'w2v'
            self.padding = True
            self.max_len = 1600
            self.emb_size = 200
        else:
            raise KeyError
        self.save_local_path = {}
        self.save_da_local_path = {}
        self.save_label_local_path = os.path.join(self.mimic3_local_data_path, 'label_%s.pt' % emb_type)
        for data_type in ['train', 'dev', 'test']:
            self.save_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s.pt' % (data_type, self.emb_type))
            self.save_da_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s_da.pt' % (data_type, self.emb_type))

    def da(self, da_type='eda'):
        self.da_type = da_type
