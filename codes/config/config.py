class RequirementConfig():
    '''
    Config for requirement
    '''
    def __init__(self):
        self.wordnet_oss_path = 'cunyin/pkgs/wordnet.zip'
        self.wordnet_local_path = './data/pkgs/wordnet.zip'
        self.stopwords_oss_path = 'cunyin/pkgs/stopwords.zip'
        self.stopwords_local_path = './data/pkgs/stopwords.zip'
        self.synonyms_oss_path = 'cunyin/pkgs/words.vector'
        self.synonyms_local_path = './data/pkgs/words.vector'
        self.owm_oss_path = 'cunyin/pkgs/omw-1.4.zip'
        self.owm_local_path = './data/pkgs/omw-1.4.zip'