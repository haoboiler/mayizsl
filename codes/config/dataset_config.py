import os


class TransICDConfig():
    def __init__(self):
        self.oss_path = 'cunyin/mimic3'  # mimic3 data
        self.local_path = './data/mimic3'
        self.NOTEEVENTS_FILE_PATH = os.path.join(self.local_path, 'NOTEEVENTS.csv')
        self.DIAGNOSES_FILE_PATH = os.path.join(self.local_path, 'DIAGNOSES_ICD.csv')
        self.PORCEDURES_FILE_PATH = os.path.join(self.local_path, 'PROCEDURES_ICD.csv')
        self.DIAG_CODE_DESC_FILE_PATH = os.path.join(self.local_path, 'D_ICD_DIAGNOSES.csv')
        self.PROC_CODE_DESC_FILE_PATH = os.path.join(self.local_path, 'D_ICD_PROCEDURES.csv')
        self.ICD_DESC_FILE_PATH = os.path.join(self.local_path, 'caml/ICD9_descriptions')
        self.GENERATED_DIR = os.path.join(self.local_path, 'generated')
        self.CAML_DIR = os.path.join(self.local_path, 'caml')


class GZSLConfig():
    def __init__(self):
        self.oss_path = 'cunyin/mimic3'  # mimic3 data
        self.local_path = './data/mimic3'
        self.processed_dir = './data/resources/processed'
        self.vocab_path = './data/resources/vocab_to_ix.pkl'


class MIMICDataConfig():
    def __init__(self, emb_type='bert'):
        # dataset output type
        self.task_type = 'Multi_Label'
        # embedding config
        self.bert_path = './pretrained_model/bert'  # bert save path
        self.bert_oss_path = 'cunyin/pretrained_model/bert_mimic'  # remote oss bert path
        self.bert_file_list = ['/config.json', '/pytorch_model.bin', '/vocab.txt', '/tokenizer_config.json']

        # w2v download path
        self.w2v_oss_path = 'cunyin/pretrained_model/w2v_mimic'
        # old setting
        '''
        self.w2v_path = './data/mimic3/generated/disch_full.w2v'
        self.w2v_model_path = './pretrained_model/w2v_embedding.pt'
        self.w2v_vocab_path = './pretrained_model/vocab.npy'
        self.w2v_local_path = './pretrained_model'
        self.w2v_file_list = ['disch_full.w2v', 'w2v_embedding.pt', 'vocab.npy']
        '''
        # pubmed setting
        self.w2v_path = './data/mimic3/pubmed.bin'
        self.w2v_bin_oss_path = os.path.join(self.w2v_oss_path, 'BioWordVec_PubMed_MIMICIII_d200.vec (1).bin')
        self.w2v_model_path = './pretrained_model/w2v_embedding_pubmed.pt'
        self.w2v_vocab_path = './pretrained_model/vocab_pubmed.npy'
        self.w2v_local_path = './pretrained_model'
        self.w2v_file_list = ['w2v_embedding_pubmed.pt', 'vocab_pubmed.npy']
        self.unk = 44973
        self.pad = 5900
        self.eos = 19527
        self.bos = 17092


        # dataset download and path config
        self.mimic3_oss_data_path = 'cunyin/mimic3/generated'
        self.mimic3_oss_emb_path = 'cunyin/mimic3/emb_output'
        self.mimic3_local_data_path = './data/mimic3'
        self.label_oss_path = os.path.join(self.mimic3_oss_data_path, 'label.csv')
        self.label_local_path = os.path.join(self.mimic3_local_data_path, 'label.csv')
        self.oss_path = {}
        self.local_path = {}
        for data_type in ['train', 'dev', 'test']:
            self.oss_path[data_type] = os.path.join(self.mimic3_oss_data_path, '%s_gzsl.csv' % data_type)
            self.local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_gzsl.csv' % data_type)
        # embedding setting
        self.embed_config(emb_type=emb_type)
        # augmentation setting
        self.da()

    def embed_config(self, emb_type='bert'):
        '''
        embedding config
        '''
        if emb_type == 'bert':
            print('use bert type')
            self.emb_type = 'bert'
            self.get_cls = False
            self.padding = True
            self.max_len = 512
            self.emb_size = 768
        elif emb_type == 'w2v':
            print('use w2v type')
            self.emb_type = 'w2v'
            self.padding = True
            self.max_len = 1500
            self.emb_size = 200
        else:
            raise KeyError
        self.save_local_path = {}
        self.save_oss_path = {}
        self.save_da_local_path = {}
        self.save_da_oss_path = {}
        self.save_label_local_path = os.path.join(self.mimic3_local_data_path, 'label_%s.pt' % emb_type)
        self.save_label_oss_path = os.path.join(self.mimic3_oss_emb_path, 'label_%s.pt' % emb_type)
        for data_type in ['train', 'dev', 'test']:
            self.save_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s.pt' % (data_type, self.emb_type))
            self.save_da_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s_da.pt' % (data_type, self.emb_type))
            self.save_oss_path[data_type] = os.path.join(self.mimic3_oss_emb_path, '%s_%s.pt' %  (data_type, self.emb_type))
            self.save_da_oss_path[data_type] = os.path.join(self.mimic3_oss_emb_path, '%s_%s_da.pt' %  (data_type, self.emb_type))
    
    def da(self, da_type='eda'):
        self.da_type = da_type


class MIMICDataConfig_Local(MIMICDataConfig):
    def __init__(self, emb_type='bert'):
        super().__init__(emb_type)
        # embedding config
        self.bert_path = './pretrained_model/bert'  # bert save path
        self.bert_file_list = ['/config.json', '/pytorch_model.bin', '/vocab.txt', '/tokenizer_config.json']

        # w2v download path
        self.w2v_model_path = './pretrained_model/w2v_mimic/w2v_embedding.pt'
        self.w2v_vocab_path = './pretrained_model/w2v_mimic/vocab.npy'

        # dataset download and path config
        self.mimic3_local_data_path = '/data/mimic3/generated'
        self.label_local_path = os.path.join(self.mimic3_local_data_path, 'label.csv')
        self.local_path = {}
        for data_type in ['train', 'dev', 'test']:
            self.local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_gzsl.csv' % data_type)
        # embedding setting
        self.embed_config(emb_type=emb_type)
        # augmentation setting
        self.da()

    def embed_config(self, emb_type='bert'):
        '''
        embedding config
        '''
        if emb_type == 'bert':
            print('use bert type')
            self.emb_type = 'bert'
            self.get_cls = False
            self.padding = True
            self.max_len = 512
            self.emb_size = 768
        elif emb_type == 'w2v':
            print('use w2v type')
            self.emb_type = 'w2v'
            self.padding = True
            self.max_len = 1600
            self.emb_size = 200
        else:
            raise KeyError
        self.save_local_path = {}
        self.save_da_local_path = {}
        self.save_label_local_path = os.path.join(self.mimic3_local_data_path, 'label_%s.pt' % emb_type)
        for data_type in ['train', 'dev', 'test']:
            self.save_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s.pt' % (data_type, self.emb_type))
            self.save_da_local_path[data_type] = os.path.join(self.mimic3_local_data_path, '%s_%s_da.pt' % (data_type, self.emb_type))

    def da(self, da_type='eda'):
        self.da_type = da_type