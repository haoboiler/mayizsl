class NewModel_Config:
    def __init__(self, class_num=8549, emb_size=768, seq_len=512, contras_t=True, contras_p=True, generate=True,
                 disperse_vq_type='Hard', distil_vq_type='Hard', vae_type='VQ_VAE', lr=0.0005, unlabel=True):
        # preprocess params:
        self.da = 'EDA'  # EDA, UDA, SimCSE
        self.class_num = class_num
        self.output_generate = False
        # vocab len
        self.vocab_len = 8824330

        # training params
        self.lr = lr
        self.batch_size = 8
        self.generate_size = 4
        self.epochs = 60

        # save params
        self.save_model_oss_path = f'cunyin/model_output/simple_model_vq_mimic/'

        # model params
        self.emb_size = emb_size  # 单个词输入的embedding size
        self.seq_len = seq_len  # transformer的最大长度
        self.dropout_rate = 0.1

        # vq vae params
        self.vae_type = vae_type
        self.distil_vq_type = distil_vq_type
        self.disperse_vq_type = disperse_vq_type

        self.disper_num = 200
        self.distil_size = 300
        self.disper_size = 300
        self.encoder_type = 'Transformer'
        self.decoder_type = 'Transformer'
        #self.decoder_type = 'DilatedCNN'
        self.vq_coef = 1
        self.comit_coef = 0.25  # commitment loss weight
        self.other_coef = 0.25

        # start encoder
        # transformer params
        self.num_heads = 8
        self.forward_expansion = 4  # transformer feedforward expansion
        self.num_layers = 2  # transformer layers
        self.pad_idx = 0

        # cnn params
        self.cnn_internel_size = 512  # internel size of cnn channel

        # descrminator
        self.discri_type = 'RelationNet'
        self.gamma = 2

        # abletion study
        self.is_contrastive_p = contras_p
        self.is_contrastive_t = contras_t
        self.is_generate_train = generate
        self.is_unlabel_train = unlabel

        self.set_model_name()

    def set_model_name(self):
        # name
        arg_list = [self.distil_vq_type, self.disperse_vq_type, self.lr, self.epochs, self.batch_size,
                    self.encoder_type, self.decoder_type, self.discri_type, self.is_contrastive_p,
                    self.is_contrastive_t, self.is_generate_train]
        self.model_name = 'model'
        for arg in arg_list:
            self.model_name = self.model_name + f'_{arg}'
        self.model_name = self.model_name + '.pt'


class NewFixSoftModel_Config(NewModel_Config):
    def __init__(self, class_num=8549, emb_size=768, seq_len=512, contras_t=True, contras_p=True, generate=True,
                 disperse_vq_type='Hard', distil_vq_type='Fix_Soft', vae_type='Fix_VQ_VAE', lr=0.0005, unlabel=True):
        # preprocess params:
        self.da = 'EDA'  # EDA, UDA, SimCSE
        self.class_num = class_num
        # 是否要把generate的句子输出
        self.output_generate = False
        # vocab len
        self.vocab_len = 8824330

        # training params
        self.lr = lr
        self.batch_size = 8
        self.generate_size = 4
        self.epochs = 60

        # save params
        self.save_model_oss_path = f'cunyin/model_output/simple_model_fix_mimic/'

        # model params
        self.emb_size = emb_size  # 单个词输入的embedding size
        self.seq_len = seq_len  # transformer的最大长度
        self.dropout_rate = 0.1

        # vq vae params
        self.vae_type = vae_type
        self.distil_vq_type = distil_vq_type
        self.disperse_vq_type = disperse_vq_type

        self.disper_num = 200
        self.distil_size = self.emb_size
        self.disper_size = 300
        self.encoder_type = 'Transformer'
        self.decoder_type = 'Transformer'
        #self.decoder_type = 'DilatedCNN'
        self.vq_coef = 1
        self.comit_coef = 0.25  # commitment loss weight
        self.other_coef = 0.25

        # start encoder
        # transformer params
        self.num_heads = 8
        self.forward_expansion = 4  # transformer feedforward expansion
        self.num_layers = 2  # transformer layers
        self.pad_idx = 0

        # cnn params
        self.cnn_internel_size = 512  # internel size of cnn channel

        # descrminator
        self.discri_type = 'SoftLinear'
        self.gamma = 2

        # abletion study
        self.is_contrastive_p = contras_p
        self.is_contrastive_t = contras_t
        self.is_generate_train = generate
        self.is_unlabel_train = unlabel

        self.set_model_name()


class TransICD_Config:
    def __init__(self, class_num=8549, emb_size=768, seq_len=512):
        # preprocess params:
        self.da = 'EDA'  # EDA, UDA, SimCSE
        self.class_num = class_num

        # training params
        self.lr = 0.001
        self.batch_size = 24
        self.generate_size = 8
        self.epochs = 60

        # save params
        self.save_model_oss_path = 'cunyin/model_output/transicd_model_mimic/'

        # model params
        self.emb_size = emb_size  # 单个词输入的embedding size
        self.seq_len = seq_len  # transformer的最大长度
        self.dropout_rate = 0.1

        # start encoder
        # transformer params
        self.num_heads = 8
        self.forward_expansion = 4  # transformer feedforward expansion
        self.num_layers = 2  # transformer layers
        self.pad_idx = 0

        # attention params
        self.attn_expansion = 2

        # name
        self.model_name = f'transicd_{self.lr}_{self.epochs}_{self.batch_size}.pt'


class FZML_Config:
    def __init__(self, class_num=8549, emb_size=768, seq_len=512):
        # preprocess params:
        self.da = 'EDA'  # EDA, UDA, SimCSE
        self.class_num = class_num

        self.adj_matrix_oss_path = 'cunyin/mimic/cut_adj_dict.pth'
        self.adj_matrix_local_path = './data/cut_adj_dict.pth'
        # training params
        self.lr = 0.001
        self.batch_size = 24
        self.generate_size = 8
        self.epochs = 40

        # save params
        self.save_model_oss_path = 'cunyin/model_output/fzml_model_mimic/'

        # model params
        self.emb_size = emb_size  # 单个词输入的embedding size
        self.seq_len = seq_len  # transformer的最大长度
        self.dropout_rate = 0.1

        # start encoder
        # transformer params
        self.num_heads = 8
        self.forward_expansion = 4  # transformer feedforward expansion
        self.num_layers = 2  # transformer layers
        self.pad_idx = 0

        # attention params
        self.attn_expansion = 2

        # name
        self.model_name = f'fzml_{self.lr}_{self.epochs}_{self.batch_size}.pt'


ModelConfig = {'New': NewModel_Config,
               'TransICD': TransICD_Config,
               'Fix': NewFixSoftModel_Config,
               'FZML': FZML_Config
               }