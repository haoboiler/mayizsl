import pandas as pd
import numpy as np
import re
from codes.config import KESUDataConfig
from codes.utils import download_file, save_data


def split_data():
    '''
    切分客诉数据集
    '''
    config = KESUDataConfig()
    clean_data_path = './data/clean_data.csv'
    clean_label_path = './data/clean_label.csv'
    # download clean text csv
    download_file('cunyin/kesu/clean_data.csv', clean_data_path)
    download_file('cunyin/kesu/clean_label.csv', clean_label_path)
    total_df = pd.read_csv(clean_data_path)
    label_df = pd.read_csv(clean_label_path)
    # split into train, test, dev
    total_df = total_df.sample(frac=1.0)
    total_df.reset_index(inplace=True)
    train_len = int(0.8 * len(total_df))
    test_len = int(0.9 * len(total_df))
    data_dict = {}
    data_dict['train'] = total_df.loc[:train_len]
    data_dict['test'] = total_df.loc[train_len+1: test_len]
    
    data_dict['dev'] = total_df.loc[test_len+1:]
    # save to oss
    for data_type in ['train', 'test', 'dev']:
        data_dict[data_type].to_csv(config.local_path[data_type])
        save_data(config.oss_path[data_type], config.local_path[data_type])


def test_label():
    clean_data_path = './data/clean_data.csv'
    clean_label_path = './data/clean_label.csv'
    # download clean text csv
    download_file('cunyin/kesu/clean_data.csv', clean_data_path)
    download_file('cunyin/kesu/clean_label.csv', clean_label_path)

    total_df = pd.read_csv(clean_data_path)
    label_df = pd.read_csv(clean_label_path)
    total_codes = list(label_df['label'])
    seen_codes_list = []
    invalid_idx_list = []
    idx = 0
    for label in total_df['label']:
        index_list = []
        if label not in total_codes:
            print(label)
        else:
            index_list.append(total_codes.index(label))
        seen_codes_list = list(set(seen_codes_list + index_list))
        if len(index_list) == 0:
            print('WARNING! no label!!!!!!!!!!!!!!!!!')
            invalid_idx_list.append(idx)
            idx += 1
            continue
        idx += 1
    return invalid_idx_list


def unlabel_data():
    clean_beetle_path = './data/beetle.csv'
    clean_biaowen_path = './data/biaowen.csv'
    # download clean text csv
    download_file('cunyin/kesu/beetle.csv', clean_beetle_path)
    download_file('cunyin/kesu/biaowen.csv', clean_biaowen_path)

    beetle_df = pd.read_csv(clean_beetle_path)
    biaowen_df = pd.read_csv(clean_biaowen_path)
    biaowen_texts = list(biaowen_df['alarm_title'])
    beetle_texts = list(beetle_df['question_title'])
    total_texts_list = []
    for biaowen in biaowen_texts:
        biaowen = biaowen[4:]
        total_texts_list.append(biaowen)
    for beetle in beetle_texts:
        total_texts_list.append(beetle)
    result_df = pd.DataFrame()
    result_df['text'] = total_texts_list
    result_df.to_csv('./data/unlabeled_data.csv')
    save_data('cunyin/kesu/unlabeled_data.csv', './data/unlabeled_data.csv')


def build_corpus():
    import jieba
    cutter = jieba.lcut()
    jieba.load_userdict('./data/kesu/keyword_list.txt')
    # download unlabel dataset
    download_file('cunyin/kesu_easy/unlabeled_data.csv', './data/unlabeled_data.csv')
    # download label dataset
    download_file('cunyin/kesu_easy/total.csv', './data/total.csv')
    text_list = []
    unlabel_df = pd.read_csv('./data/unlabeled_data.csv')
    label_df = pd.read_csv('./data/total.csv')
 
    
def cut_all_sentences():
    import jieba
    jieba.load_userdict('./data/kesu/keyword_list.txt')
    cutter = jieba.lcut

    # download unlabel dataset
    download_file('cunyin/kesu_easy/unlabeled_data.csv', './data/unlabeled_data.csv')
    # download label dataset
    download_file('cunyin/kesu_easy/total.csv', './data/total.csv')
    download_file('cunyin/kesu_easy/clean_label.csv', './data/clean_label.csv')
    unlabel_df = pd.read_csv('./data/unlabeled_data.csv')
    label_df = pd.read_csv('./data/total.csv')
    label = pd.read_csv('./data/clean_label.csv')
    vocab_dict = np.load('../vocab.npy', allow_pickle=True).item()
    split_length_list = []
    for text in label['label']:
        x = cutter(text)
        jieba_word = []
        for word in x:
            word = word.lower()
            if word in vocab_dict.keys():
                jieba_word.append(word)
            else:
                new_cut = jieba.cut_for_search(word)
                new_list = []
                for little_word in new_cut:
                    if little_word in vocab_dict.keys():
                        new_list.append(little_word)
                        jieba_word.append(little_word)
                    print('new list', new_list)
                    new_word_list = re.split("|".join(new_list), word)
                    for word_l3 in new_word_list:
                        if word_l3 in vocab_dict.keys():
                            jieba_word.append(word_l3)
                        else:
                            jieba_word.append('unk')
        print(text)
        print(jieba_word)
        split_length_list.append(len(jieba_word))
    
    for text in unlabel_df['text']:
        x = cutter(text)
        jieba_word = []
        for word in x:
            word = word.lower()
            if word in vocab_dict.keys():
                jieba_word.append(word)
            else:
                new_cut = jieba.cut_for_search(word)
                new_list = []
                for little_word in new_cut:
                    if little_word in vocab_dict.keys():
                        new_list.append(little_word)
                        jieba_word.append(little_word)
                    print('new list', new_list)
                    new_word_list = re.split("|".join(new_list), word)
                    for word_l3 in new_word_list:
                        if word_l3 in vocab_dict.keys():
                            jieba_word.append(word_l3)
                        else:
                            jieba_word.append('unk')
                        
                        
                jieba_word.append(['unk', new_list])
        print(text)
        print(jieba_word)
        split_length_list.append(len(jieba_word))
    for text in label_df['data']:
        x = cutter(text)
        jieba_word = []
        for word in x:
            word = word.lower()
            if word in vocab_dict.keys():
                jieba_word.append(word)
            else:
                new_cut = jieba.cut_for_search(word)
                new_list = []
                for little_word in new_cut:
                    if little_word in vocab_dict.keys():
                        new_list.append(little_word)
                        jieba_word.append(little_word)
                    print('new list', new_list)
                    new_word_list = re.split("|".join(new_list), word)
                    for word_l3 in new_word_list:
                        if word_l3 in vocab_dict.keys():
                            jieba_word.append(word_l3)
                        else:
                            jieba_word.append('unk')
                jieba_word.append(['unk', new_list])
        print(text)
        print(jieba_word)
        split_length_list.append(len(jieba_word))
    
    df = pd.DataFrame()
    df['length'] = split_length_list
    print(df['length'].describe())


def build_yuque():
    label_df = pd.read_csv('../data/kesu/clean_label.csv')
    label_list = list(label_df['label'])
    yuque_df = pd.read_csv('../data/kesu/yuque.csv')
    id_list = []
    for label in yuque_df['label']:
        try:
            idx = label_list.index('蚂蚁集团/' + label.strip())
            id_list.append(idx)
            print('in label', label)
        except Exception as e:
            #print(e, 'no label', label)
            pass
    choosed_id = list(set(id_list))
    choosed_id.sort()
    print(label_df['label'][choosed_id])


def build_label_matrix():
    df =  pd.read_csv('../data/kesu/clean_label.csv')

    label_list = list(df['label'])
    
    label_df = pd.DataFrame()
    label_id_list = []
    label_parent_list = []
    label_name_list = []
    # deal with label parent
    for i in range(len(label_list)):
        label_id_list.append(i)
        label_name_list.append(label_list[i])
        try:
            tmp = label_list[i].split('/')[:-1]
            print('/'.join(tmp))
            parent_id = label_list.index('/'.join(tmp))
            label_parent_list.append(parent_id)
        except:
            label_parent_list.append(np.nan)
    label_df['id'] = label_id_list
    label_df['label'] = label_name_list
    label_df['description'] = label_name_list
    label_df['parent'] = label_parent_list

    label_df.to_csv('../data/kesu/label.csv', index=False)
    return label_df

def split_dataset():
    total_df = pd.read_csv('../data/kesu/total.csv')
    label_list = list(total_df['label'].unique())
    # print(label_list)
    import random
    # random sample half label as test dataset
    train_label_list = random.sample(label_list, int(len(label_list)*2/3))
    test_label_list = [i for i in label_list if i not in train_label_list]
    print(train_label_list)
    print('########')
    print(test_label_list)
    train_df = total_df[total_df['label'].isin(train_label_list)].reset_index(drop=True)
    test_df = total_df[total_df['label'].isin(test_label_list)].reset_index(drop=True)
    train_index_list = []
    eval_index_list = []
    for label in train_label_list:
        tmp_df = train_df[train_df['label'] == label]
        if len(tmp_df) >= 2:
            index_list = list(tmp_df.index)
            random.shuffle(index_list)
            eval_index_list = eval_index_list + index_list[int(len(index_list)*2/3):]
            train_index_list = train_index_list + index_list[:int(len(index_list)*2/3)]
    train_index_list.sort()
    eval_index_list.sort()
    print(train_index_list)
    print(eval_index_list)
    eval_df = train_df.iloc[eval_index_list]
    train_df = train_df.iloc[train_index_list].reset_index(drop=True)
    

    test_index_list = []
    eval_index_list = []
    for label in test_label_list:
        tmp_df = test_df[test_df['label'] == label]
        if len(tmp_df) >= 2:
            index_list = list(tmp_df.index)
            random.shuffle(index_list)
            eval_index_list = eval_index_list + index_list[int(len(index_list)*2/3):]
            test_index_list = test_index_list + index_list[:int(len(index_list)*2/3)]
    eval_df = eval_df.append(test_df.iloc[eval_index_list]).reset_index()
    test_df = test_df.iloc[test_index_list].reset_index(drop=True)
    


    train_df.to_csv('../data/kesu/train.csv')
    test_df.to_csv('../data/kesu/test.csv')
    eval_df.to_csv('../data/kesu/eval.csv')
    

def clean_da_data():
    total_df = pd.read_csv('../data/kesu/total.csv')
    da_data = []
    for data in total_df['da_data']:
        da_data.append(data.replace(' ',''))
    label_list = []
    for label in total_df['label']:
        label_list.append(label[5:])
    total_df['da_data'] = da_data
    total_df['label'] = label_list
    total_df.to_csv('../data/kesu/total.csv', index=False)


    
def build_easy_label():
    label_df = pd.read_csv('../data/kesu/label.csv')
    total_df = pd.read_csv('../data/kesu/total.csv')
    # drop majiyituan/
    label_list = []
    for label in label_df['label']:
        label_list.append(label[5:])
    label_df['label'] = label_list
    
    label_df = label_df[label_df['label'].isin(total_df['label'])]
    label_df = label_df.reset_index(drop=True)
    label_df['id'] = label_df.index
    label_list = list(label_df['label'])
    
    label_df = pd.DataFrame()
    label_id_list = []
    label_parent_list = []
    label_name_list = []
    # deal with label parent
    for i in range(len(label_list)):
        label_id_list.append(i)
        label_name_list.append(label_list[i])
        try:
            tmp = label_list[i].split('/')[:-1]
            print('/'.join(tmp))
            parent_id = label_list.index('/'.join(tmp))
            label_parent_list.append(parent_id)
        except:
            label_parent_list.append(np.nan)
    label_df['id'] = label_id_list
    label_df['label'] = label_name_list
    label_df['description'] = label_name_list
    label_df['parent'] = label_parent_list

    label_df.to_csv('../data/kesu/simple_label.csv')


    label_list = list(total_df['label'].unique())
    # print(label_list)
    import random
    # random sample half label as test dataset
    train_label_list = random.sample(label_list, int(len(label_list)*2/3))
    test_label_list = [i for i in label_list if i not in train_label_list]
    print(train_label_list)
    print('########')
    print(test_label_list)
    train_df = total_df[total_df['label'].isin(train_label_list)].reset_index(drop=True)
    test_df = total_df[total_df['label'].isin(test_label_list)].reset_index(drop=True)
    train_index_list = []
    eval_index_list = []
    for label in train_label_list:
        tmp_df = train_df[train_df['label'] == label]
        if len(tmp_df) >= 2:
            index_list = list(tmp_df.index)
            random.shuffle(index_list)
            eval_index_list = eval_index_list + index_list[int(len(index_list)*2/3):]
            train_index_list = train_index_list + index_list[:int(len(index_list)*2/3)]
    train_index_list.sort()
    eval_index_list.sort()
    print(train_index_list)
    print(eval_index_list)
    eval_df = train_df.iloc[eval_index_list]
    train_df = train_df.iloc[train_index_list].reset_index(drop=True)
    

    test_index_list = []
    eval_index_list = []
    for label in test_label_list:
        tmp_df = test_df[test_df['label'] == label]
        if len(tmp_df) >= 2:
            index_list = list(tmp_df.index)
            random.shuffle(index_list)
            eval_index_list = eval_index_list + index_list[int(len(index_list)*2/3):]
            test_index_list = test_index_list + index_list[:int(len(index_list)*2/3)]
    eval_df = eval_df.append(test_df.iloc[eval_index_list]).reset_index(drop=True)
    test_df = test_df.iloc[test_index_list].reset_index(drop=True)
    


    train_df.to_csv('../data/kesu/train.csv', index=False)
    test_df.to_csv('../data/kesu/test.csv', index=False)
    eval_df.to_csv('../data/kesu/eval.csv', index=False)
    