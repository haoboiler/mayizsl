import gensim
from gensim.models import Word2Vec
import logging
import pandas as pd
import numpy as np
from codes.config import TransICDConfig, MIMICDataConfig, KESUDataConfig
from gensim.models import Word2Vec, KeyedVectors
import multiprocessing
import os
from codes.utils.download import download_file
from codes.utils.upload import save_data
import torch
import jieba
jieba.load_userdict('./data/kesu/keyword_list.txt')


def pubmed():
    config = MIMICDataConfig()
    download_file(config.w2v_bin_oss_path, config.w2v_path)
    model = KeyedVectors.load_word2vec_format(config.w2v_path, binary=True)
    weights = torch.FloatTensor(model.wv.vectors)
    np.save(config.w2v_vocab_path, model.wv.key_to_index)
    torch.save(weights, config.w2v_model_path)
    for file in config.w2v_file_list:
        save_data(os.path.join(config.w2v_oss_path, file), os.path.join(config.w2v_local_path, file))
    strings = ['unk', '<unk>', 'UNK', '<UNK>', 'bos', '<bos>', 'BOS', '<BOS>', 'eos', '<eos>', 'EOS', '<EOS>', 'pad', '<pad>', 'PAD', '<PAD>']
    for string in strings:
        try:
            model.key_to_index[string]
            print(string, model.key_to_index[string])
            print(model[string])
        except:
            print('no', string) 


def ailab():
    config = KESUDataConfig()
    download_file(config.w2v_oss_path + '/vocab_big.npy', config.w2v_vocab_path)
    vocab_dict = np.load(config.w2v_vocab_path, allow_pickle=True).item()
    strings = ['unk', '<unk>', 'UNK', '<UNK>', 'bos', '<bos>', 'BOS', '<BOS>', 'eos', '<eos>', 'EOS', '<EOS>', 'pad', '<pad>', 'PAD', '<PAD>']
    for string in strings:
        try:
            vocab_dict[string]
            print(string, vocab_dict[string])
        except:
            print('no', string)


def ailab_small():
    config = KESUDataConfig()
    download_file('cunyin/pretrained_model/w2v_yuque/70000-small.txt', './data/ailab_w2v')
    model = KeyedVectors.load_word2vec_format('./data/ailab_w2v')
    weights = torch.FloatTensor(model.vectors)
    np.save(config.w2v_vocab_path, model.key_to_index)
    torch.save(weights, config.w2v_model_path)
    for file in config.w2v_file_list:
        save_data(os.path.join(config.w2v_oss_path, file), os.path.join(config.w2v_local_path, file))
    strings = ['unk', '<unk>', 'UNK', '<UNK>', 'sos', '<bos>', 'BOS', '<BOS>', 'eos', '<eos>', 'EOS', '<EOS>', 'pad', '<pad>', 'PAD', '<PAD>', '花呗', '花', '呗']
    for string in strings:
        try:
            model.key_to_index[string]
            print(string, model.key_to_index[string])
            print(model[string])
        except:
            print('no', string)


def get_kesu_word_dict_old():
    # get all words
    word_list = []
    download_file('cunyin/kesu/unlabeled_data.csv', './data/unlabeled_data.csv')
    download_file('cunyin/kesu/clean_data.csv', './data/clean_data.csv')
    download_file('cunyin/kesu/clean_label.csv', './data/clean_label.csv')
    unlabel_df = pd.read_csv('./data/unlabeled_data.csv')
    label_df = pd.read_csv('./data/clean_label.csv')
    text_df = pd.read_csv('./data/clean_data.csv')
    for sentence in unlabel_df['text']:
        words = jieba.cut(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
    for sentence in label_df['label']:
        words = jieba.cut(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
    for sentence in text_df['data']:
        words = jieba.cut(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
    print(len(word_list))
    return word_list


def get_kesu_word_dict():
    # get all words
    word_list = []
    download_file('cunyin/kesu_easy/unlabeled_data.csv', './data/unlabeled_data.csv')
    download_file('cunyin/kesu_easy/total.csv', './data/total.csv')
    download_file('cunyin/kesu_easy/clean_label.csv', './data/clean_label.csv')
    unlabel_df = pd.read_csv('./data/unlabeled_data.csv')
    label_df = pd.read_csv('./data/clean_label.csv')
    text_df = pd.read_csv('./data/total.csv')
    for sentence in unlabel_df['text']:
        words = jieba.cut_for_search(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
            for i in range(len(word)):
                a = word[i]
                if a not in word_list:
                    word_list.append(a)
    for sentence in unlabel_df['da_text']:
        words = jieba.cut_for_search(sentence.replace(' ', ''))
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
            for i in range(len(word)):
                a = word[i]
                if a not in word_list:
                    word_list.append(a)
    for sentence in label_df['label']:
        words = jieba.cut_for_search(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
            for i in range(len(word)):
                a = word[i]
                if a not in word_list:
                    word_list.append(a)
    for sentence in text_df['data']:
        words = jieba.cut_for_search(sentence)
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
            for i in range(len(word)):
                a = word[i]
                if a not in word_list:
                    word_list.append(a)
    for sentence in text_df['da_data']:
        words = jieba.cut_for_search(sentence.replace(' ', ''))
        for word in words:
            word = word.lower()
            if word not in word_list:
                word_list.append(word)
            for i in range(len(word)):
                a = word[i]
                if a not in word_list:
                    word_list.append(a)
    print(len(word_list))
    #print(word_list)
    return word_list


def build_ailab_small():
    config = KESUDataConfig()
    # build big dict
    #download_file('cunyin/pretrained_model/w2v_yuque/vocab_big.npy', './data/vocab_big.npy')
    #download_file('cunyin/pretrained_model/w2v_yuque/w2v_embedding_big.pt', './data/w2v_embedding_big.pt')
    big_vocab_dict = np.load('./data/vocab_big.npy', allow_pickle=True).item()
    big_weight = torch.load('./data/w2v_embedding_big.pt')
    # build small dict
    download_file('cunyin/pretrained_model/w2v_yuque/70000-small.txt', './data/ailab_w2v')
    model = KeyedVectors.load_word2vec_format('./data/ailab_w2v')
    weights = torch.FloatTensor(model.vectors)
    # word_list
    word_list = get_kesu_word_dict()
    word_list.append('unk')
    word_list.append('bos')
    final_word_dict = model.key_to_index
    unseen_word_idx = []
    for word in word_list:
        if word not in final_word_dict.keys():
            if word not in big_vocab_dict.keys():
                print('both not word', word)
            else:
                unseen_word_idx.append(big_vocab_dict[word])
                final_word_dict[word] = len(final_word_dict)
    idx = torch.tensor(unseen_word_idx)
    idx_weight = big_weight[idx]
    print(idx_weight.shape)
    final_weight = torch.cat([weights, idx_weight], dim=0)
    print(final_weight.shape)
    print(len(final_word_dict))
    np.save(config.w2v_vocab_path, final_word_dict)
    torch.save(final_weight, config.w2v_model_path)
    for file in config.w2v_file_list:
        save_data(os.path.join(config.w2v_oss_path, file), os.path.join(config.w2v_local_path, file))
    strings = ['unk', '<unk>', 'UNK', '<UNK>', 'bos', '<bos>', 'BOS', '<BOS>', 'eos', '<eos>', 'EOS', '<EOS>', 'pad', '<pad>', 'PAD', '<PAD>']
    for string in strings:
        try:
            model.key_to_index[string]
            print(string, model.key_to_index[string])
            #print(model[string])
        except:
            print('no', string)
            
            

def build_ailab_smallest():
    config = KESUDataConfig()
    # build big dict
    #download_file('cunyin/pretrained_model/w2v_yuque/vocab_big.npy', './data/vocab_big.npy')
    #download_file('cunyin/pretrained_model/w2v_yuque/w2v_embedding_big.pt', './data/w2v_embedding_big.pt')
    big_vocab_dict = np.load('./data/vocab_big.npy', allow_pickle=True).item()
    big_weight = torch.load('./data/w2v_embedding_big.pt')

    # word_list
    word_list = get_kesu_word_dict()
    word_list.append('mask')
    word_list.append('unk')
    word_list.append('bos')
    word_list.append('eos')
    word_list.append('pad')
    word_list = list(set(word_list))
    print(len(word_list))
    unseen_word_idx = []
    final_word_dict = {}
    for word in word_list:
        if word not in big_vocab_dict.keys():
            #print('both not word', word)
            pass
        else:
            unseen_word_idx.append(big_vocab_dict[word])
            final_word_dict[word] = len(final_word_dict)
    print(final_word_dict['mask'])
    idx = torch.tensor(unseen_word_idx)
    final_weight = big_weight[idx]
    print(len(final_word_dict))
    np.save(config.w2v_vocab_path, final_word_dict)
    torch.save(final_weight, config.w2v_model_path)
    for file in config.w2v_file_list:
        save_data(os.path.join(config.w2v_oss_path, file), os.path.join(config.w2v_local_path, file))


def test_ailab():
    #download_file('cunyin/pretrained_model/w2v_yuque/vocab_big.npy', './data/vocab_big.npy')
    #download_file('cunyin/pretrained_model/w2v_yuque/w2v_embedding_big.pt', './data/w2v_embedding_big.pt')
    big_vocab_dict = np.load('./data/vocab_big.npy', allow_pickle=True).item()
    big_weight = torch.load('./data/w2v_embedding_big.pt')

    download_file('cunyin/pretrained_model/w2v_yuque/vocab.npy', './data/vocab.npy')
    download_file('cunyin/pretrained_model/w2v_yuque/w2v_embedding.pt', './data/w2v_embedding.pt')
    vocab_dict = np.load('./data/vocab.npy', allow_pickle=True).item()
    weight = torch.load('./data/w2v_embedding.pt')

    key = list(vocab_dict.keys())[-1]
    print(key, len(vocab_dict.keys()))
    vocab_idx = vocab_dict[key]
    big_idx = big_vocab_dict[key]
    small_vector = weight[torch.tensor([vocab_idx])]
    print(small_vector)
    big_vector = big_weight[torch.tensor([big_idx])]
    print(big_vector)
