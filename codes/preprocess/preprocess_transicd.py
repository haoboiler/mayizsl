
import gensim
from gensim.models import Word2Vec
import logging
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from codes.config import TransICDConfig, MIMICDataConfig
import re
import string
from gensim.models import Word2Vec
import multiprocessing
from collections import defaultdict, Counter
import csv
import os
from codes.utils.download import download_file, download_mimic3
from codes.utils.upload import save_data
import nltk
nltk.data.path.append("./data/pkgs")
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
import torch

CONFIG = TransICDConfig()
my_stopwords = set([stopword for stopword in stopwords.words('english')])
my_stopwords.update({'admission', 'birth', 'date', 'discharge', 'service', 'sex', 'patient', 'name', 'history',
                     'hospital', 'last', 'first', 'course', 'past', 'day', 'one', 'family', 'chief', 'complaint'})
stemmer = SnowballStemmer('english')
punct = string.punctuation.replace('-', '') + ''.join(["``", "`", "..."])
trantab = str.maketrans(punct, len(punct) * ' ')


# Credit: https://github.com/jamesmullenbach/caml-mimic
def reformat(code, is_diag):
    """
        Put a period in the right place because the MIMIC-3 data files exclude them.
        Generally, procedure codes have dots after the first two digits,
        while diagnosis codes have dots after the first three digits.
    """
    code = ''.join(code.split('.'))
    if is_diag:
        if code.startswith('E'):
            if len(code) > 4:
                code = code[:4] + '.' + code[4:]
        else:
            if len(code) > 3:
                code = code[:3] + '.' + code[3:]
    else:
        code = code[:2] + '.' + code[2:]
    return code


def combine_diag_proc_codes(hadm_id_set, out_filename='ALL_CODES_filtered.csv'):
    logging.info("Started Preprocessing raw MIMIC-III data")
    diag_df = pd.read_csv(CONFIG.DIAGNOSES_FILE_PATH, dtype={"ICD9_CODE": str})
    proc_df = pd.read_csv(CONFIG.PORCEDURES_FILE_PATH, dtype={"ICD9_CODE": str})

    diag_df['ICD9_CODE'] = diag_df['ICD9_CODE'].apply(lambda code: str(reformat(str(code), True)))
    proc_df['ICD9_CODE'] = proc_df['ICD9_CODE'].apply(lambda code: str(reformat(str(code), False)))
    codes_df = pd.concat([diag_df, proc_df], ignore_index=True)
    num_original_hadm_id = len(codes_df['HADM_ID'].unique())
    logging.info(f'Total unique HADM_ID (original): {num_original_hadm_id}')

    codes_df = codes_df[codes_df['HADM_ID'].isin(hadm_id_set)]
    codes_df.sort_values(['SUBJECT_ID', 'HADM_ID'], inplace=True)
    num_filtered_hadm_id = len(codes_df['HADM_ID'].unique())
    logging.info(f'Total unique HADM_ID (ALL_CODES_filtered): {num_filtered_hadm_id}')
    num_unique_codes = len(codes_df['ICD9_CODE'].unique())
    logging.info(f'Total unique ICD9_CODE (ALL_CODES_filtered): {num_unique_codes}')
    codes_df.to_csv(f'{CONFIG.GENERATED_DIR}/{out_filename}', index=False,
                    columns=['SUBJECT_ID', 'HADM_ID', 'ICD9_CODE'],
                    header=['SUBJECT_ID', 'HADM_ID', 'ICD9_CODE'])

    return out_filename


def clean_text(text, trantab, my_stopwords=None, stemmer=None):
    text = text.lower().translate(trantab)
    tokens = text.strip().split()

    if stemmer:
        tokens = [stemmer.stem(t) for t in tokens]

    tokens = [token for token in tokens if not token.isnumeric() and len(token) > 2]

    if my_stopwords:
        tokens = [x for x in tokens if x not in my_stopwords]

    text = ' '.join(tokens)
    text = re.sub('-', '', text)
    text = re.sub('\d+\s', ' ', text)
    text = re.sub('\d', 'n', text)
    text = '<BOS> ' + text + ' <EOS>'
    return text


def write_discharge_summaries(out_filename='disch_full.csv'):
    logging.info("processing notes file")

    disch_df = pd.read_csv(CONFIG.NOTEEVENTS_FILE_PATH)
    selected_categories = ['Discharge summary']
    disch_df = disch_df[disch_df['CATEGORY'].isin(selected_categories)]
    disch_df['TEXT'] = disch_df['TEXT'].apply(lambda text: clean_text(text, trantab, my_stopwords, stemmer))

    disch_df.sort_values(['SUBJECT_ID', 'HADM_ID'], inplace=True)
    logging.info(f'Total number of rows: {len(disch_df)}')
    hadm_id_set = set(disch_df['HADM_ID'])
    logging.info(f'Total number of unique HADM_ID (disch_full): {len(hadm_id_set)}')
    disch_df.to_csv(f'{CONFIG.GENERATED_DIR}/{out_filename}', index=False,
                    columns=['SUBJECT_ID', 'HADM_ID', 'CHARTTIME', 'TEXT'],
                    header=['SUBJECT_ID', 'HADM_ID', 'CHARTTIME', 'TEXT'])

    return hadm_id_set, out_filename


def combine_notes_codes(disch_full_filename, filtered_codes_filename, out_filename='notes_labeled.csv'):
    logging.info('Merging discharge summary and ICD codes')
    disch_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{disch_full_filename}')
    disch_grouped = disch_df.groupby('HADM_ID')['TEXT'].apply(lambda texts: ' '.join(texts))
    disch_df = pd.DataFrame(disch_grouped)
    disch_df.reset_index(inplace=True)

    codes_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{filtered_codes_filename}', dtype={"ICD9_CODE": str})
    codes_grouped = codes_df.groupby('HADM_ID')['ICD9_CODE'].apply(lambda codes: ';'.join(map(str, codes)))
    codes_df = pd.DataFrame(codes_grouped)
    codes_df.reset_index(inplace=True)

    merged_df = pd.merge(disch_df, codes_df, on='HADM_ID')
    merged_df.sort_values(['HADM_ID'], inplace=True)
    num_hadm_id = len(merged_df['HADM_ID'].unique())
    logging.info(f'Total number of unique HADM_ID (merged): {num_hadm_id}')

    merged_df.to_csv(f'{CONFIG.GENERATED_DIR}/{out_filename}', index=False,
                     columns=['HADM_ID', 'TEXT', 'ICD9_CODE'],
                     header=['HADM_ID', 'TEXT', 'LABELS'])

    return out_filename


def split_data(labeled_notes_filename='notes_labeled.csv', is_full=True):
    labeled_notes_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{labeled_notes_filename}')
    counter = Counter()
    for labels in labeled_notes_df['LABELS'].values:
        for label in str(labels).split(';'):
            counter[label] += 1

    codes, freqs = map(list, zip(*counter.most_common()))
    code_freq_df = pd.DataFrame({'code': codes, 'freq': freqs})
    code_freq_filename = 'code_freq.csv'
    code_freq_df.to_csv(f'{CONFIG.GENERATED_DIR}/{code_freq_filename}', index=False)

    if is_full:
        content = 'full'
        for split in ['train', 'dev', 'test']:
            split_hadm_id_df = pd.read_csv(f'{CONFIG.CAML_DIR}/{split}_{content}_hadm_ids.csv', names=['HADM_ID'])
            split_hadm_ids = split_hadm_id_df['HADM_ID'].values.tolist()

            split_labeled_notes_df = labeled_notes_df[labeled_notes_df['HADM_ID'].isin(split_hadm_ids)]
            split_labeled_notes_df['LENGTH'] = split_labeled_notes_df['TEXT'].apply(lambda text: len(str(text).split()))
            split_labeled_notes_df = split_labeled_notes_df.sort_values(['LENGTH'], ascending=False)
            logging.info(f'Total rows in {split}_{content}.csv: {len(split_labeled_notes_df)}')
            split_labeled_notes_df.to_csv(f'{CONFIG.GENERATED_DIR}/{split}_{content}.csv', index=False)
    else:
        content = 50
        top_codes = set(codes[:content])

        for split in ['train', 'dev', 'test']:
            split_hadm_id_df = pd.read_csv(f'{CONFIG.CAML_DIR}/{split}_{content}_hadm_ids.csv', names=['HADM_ID'])
            split_hadm_ids = split_hadm_id_df['HADM_ID'].values.tolist()

            split_labeled_notes_df = labeled_notes_df[labeled_notes_df['HADM_ID'].isin(split_hadm_ids)]
            split_labeled_notes_df['LABELS'] = split_labeled_notes_df['LABELS'].apply(lambda codes: ';'.join(top_codes.intersection(set(codes.split(';')))))
            split_labeled_notes_df = split_labeled_notes_df[split_labeled_notes_df['LABELS'].str.len() > 0]

            split_labeled_notes_df['LENGTH'] = split_labeled_notes_df['TEXT'].apply(lambda text: len(str(text).split()))
            split_labeled_notes_df = split_labeled_notes_df.sort_values(['LENGTH'], ascending=False)
            logging.info(f'Total rows in {split}_{content}.csv: {len(split_labeled_notes_df)}')
            split_labeled_notes_df.to_csv(f'{CONFIG.GENERATED_DIR}/{split}_{content}.csv', index=False)


def split_data_gzsl(labeled_notes_filename='notes_labeled.csv'):
    labeled_notes_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{labeled_notes_filename}')
    counter = Counter()
    for labels in labeled_notes_df['LABELS'].values:
        for label in str(labels).split(';'):
            counter[label] += 1

    codes, freqs = map(list, zip(*counter.most_common()))
    code_freq_df = pd.DataFrame({'code': codes, 'freq': freqs})
    code_freq_filename = 'code_freq.csv'
    code_freq_df.to_csv(f'{CONFIG.GENERATED_DIR}/{code_freq_filename}', index=False)

    for split in ['train', 'dev', 'test']:
        split_hadm_id_df = pd.read_csv(f'{CONFIG.CAML_DIR}/{split}_thresh5_hdam_ids.txt', names=['HADM_ID'])
        split_hadm_ids = split_hadm_id_df['HADM_ID'].values.tolist()
        split_hadm_ids = [a.split('_')[1] for a in split_hadm_ids]

        split_labeled_notes_df = labeled_notes_df[labeled_notes_df['HADM_ID'].isin(split_hadm_ids)]
        split_labeled_notes_df['LENGTH'] = split_labeled_notes_df['TEXT'].apply(lambda text: len(str(text).split()))
        split_labeled_notes_df = split_labeled_notes_df.sort_values(['LENGTH'], ascending=False)
        logging.info(f'Total rows in {split}_gzsl.csv: {len(split_labeled_notes_df)}')
        split_labeled_notes_df.to_csv(f'{CONFIG.GENERATED_DIR}/{split}_gzsl.csv', index=False)
    save_data('cunyin/mimic3/generated/dev_gzsl.csv', './data/mimic3/generated/dev_gzsl.csv')
    save_data('cunyin/mimic3/generated/train_gzsl.csv', './data/mimic3/generated/train_gzsl.csv')
    save_data('cunyin/mimic3/generated/test_gzsl.csv', './data/mimic3/generated/test_gzsl.csv')


def preprocess_icd9():
    icd9_path = os.path.join(CONFIG.CAML_DIR, 'ICD9_descriptions')
    with open(icd9_path, 'r') as f:
        text_list = f.readlines()
    code_list = []
    description_list = []
    for text in text_list:
        text = text[:-1]
        text = text.split('	')
        if len(text) != 2:
            continue
        code = text[0]
        text = text[1]
        if '-' in code or '@' in code:
            continue
        else:
            code_list.append(code)
            description_list.append(text)
    df = pd.DataFrame()
    df['code'] = code_list
    df['text'] = description_list
    save_path = os.path.join(CONFIG.GENERATED_DIR, 'label.csv')
    df.to_csv(save_path)
    save_data('cunyin/mimic3/generated/label.csv', save_path)


def load_code_desc():
    desc_dict = defaultdict(str)
    with open(CONFIG.DIAG_CODE_DESC_FILE_PATH, 'r') as descfile:
        r = csv.reader(descfile)
        # header
        next(r)
        for row in r:
            code = row[1]
            desc = row[-1]
            desc_dict[reformat(code, True)] = desc
    with open(CONFIG.PROC_CODE_DESC_FILE_PATH, 'r') as descfile:
        r = csv.reader(descfile)
        # header
        next(r)
        for row in r:
            code = row[1]
            desc = row[-1]
            if code not in desc_dict.keys():
                desc_dict[reformat(code, False)] = desc
    with open(CONFIG.ICD_DESC_FILE_PATH, 'r') as labelfile:
        for i,row in enumerate(labelfile):
            row = row.rstrip().split()
            code = row[0]
            if code not in desc_dict.keys():
                desc_dict[code] = ' '.join(row[1:])
    return desc_dict


def build_vocab(train_full_filename='train_full.csv', out_filename='vocab.csv'):
    train_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{train_full_filename}')
    desc_dt = load_code_desc()
    desc_series = pd.Series(list(desc_dt.values())).apply(lambda text: clean_text(text, trantab, my_stopwords, stemmer))

    full_text_series = train_df['TEXT'].append(desc_series, ignore_index=True)
    cv = CountVectorizer(min_df=1)
    cv.fit(full_text_series)

    out_file_path = f'{CONFIG.GENERATED_DIR}/{out_filename}'
    with open(out_file_path, 'w') as fout:
        for word in cv.get_feature_names():
            fout.write(f'{word}\n')


def embed_words(disch_full_filename='disch_full.csv', embed_size=200, out_filename='disch_full.w2v'):
    '''
    word embedding
    '''
    disch_df = pd.read_csv(f'{CONFIG.GENERATED_DIR}/{disch_full_filename}')
    sentences = [text.split() for text in disch_df['TEXT']]
    desc_dt = load_code_desc()
    for desc in desc_dt.values():
        sentences.append(clean_text(desc, trantab, my_stopwords, stemmer).split())

    num_cores = multiprocessing.cpu_count()
    min_count = 0
    window = 5
    num_negatives = 5
    logging.info('\n**********************************************\n')
    logging.info('Training CBOW embedding...')
    logging.info(f'Params: embed_size={embed_size}, workers={num_cores-1}, min_count={min_count}, window={window}, negative={num_negatives}')
    w2v_model = Word2Vec(min_count=min_count, window=window, vector_size=embed_size, negative=num_negatives, workers=num_cores-1)
    w2v_model.build_vocab(sentences, progress_per=10000)
    w2v_model.train(sentences, total_examples=w2v_model.corpus_count, epochs=30, report_delay=1)
    w2v_model.init_sims(replace=True)
    w2v_model.save(f'{CONFIG.GENERATED_DIR}/{out_filename}')
    logging.info('\n**********************************************\n')
    return out_filename


def save_model(config):
    model = gensim.models.Word2Vec.load(config.w2v_path)
    weights = torch.FloatTensor(model.wv.vectors)
    np.save(config.w2v_vocab_path, model.wv.key_to_index)
    torch.save(weights, config.w2v_model_path)
    for file in config.w2v_file_list:
        save_data(os.path.join(config.w2v_oss_path, file), os.path.join(config.w2v_local_path, file))


def get_vectors(config):
    
    max_len = config.max_len
    print('max len is', max_len)
    emb_size = config.emb_size
    model = gensim.models.Word2Vec.load(config.w2v_path)
    pad_embed = np.zeros(emb_size)
    unk_embed = np.random.randn(emb_size)
    for dataset in ['train', 'test', 'dev']:
        print('start', dataset)
        download_file(config.oss_path[dataset], config.local_path[dataset])
        local_path = config.local_path[dataset]
        df = pd.read_csv(local_path)
        result = []
        for text in df['TEXT']:
            text_list = text.split()
            one_torch = []
            one_torch.append(model.wv['<BOS>'])
            for word in text_list:
                if len(one_torch) < max_len - 1:
                    pass
                else:
                    break
                try:
                    vec = model.wv[word]
                    one_torch.append(vec)
                except Exception as _:
                    one_torch.append(unk_embed)
            while (len(one_torch) < max_len - 1):
                one_torch.append(pad_embed)
            one_torch.append(model.wv['<EOS>'])
            one_torch = np.array(one_torch)
            #print(len(one_torch))
            result.append(one_torch)
            print(len(result))
        result = np.array(result)
        print(len(result))
        # save to oss
        torch.save(result, config.save_local_path[dataset])
        save_data(config.save_oss_path[dataset], config.save_local_path[dataset])            


def main(retrain_w2v=False):
    # 创建dir
    if not os.path.exists('./data/mimic3/results'):
        os.makedirs('./data/mimic3/results')
    if not os.path.exists(CONFIG.GENERATED_DIR):
        os.makedirs(CONFIG.GENERATED_DIR)
    if not os.path.exists(CONFIG.CAML_DIR):
        os.makedirs(CONFIG.CAML_DIR)

    # get config
    dataset_config = MIMICDataConfig(emb_type='w2v')

    #FORMAT = '%(asctime)-15s %(message)s'
    #logging.basicConfig(filename='./data/mimic3/results/preprocess.log', filemode='w', format=FORMAT, level=logging.INFO)
    #hadm_id_set, disch_full_filename = write_discharge_summaries()
    #filtered_codes_filename = combine_diag_proc_codes(hadm_id_set)
    # 下载已经完成的文件
    download_file('cunyin/mimic3/generated/disch_full.csv', './data/mimic3/generated/disch_full.csv')
    download_file('cunyin/mimic3/generated/ALL_CODES_filtered.csv', './data/mimic3/generated/ALL_CODES_filtered.csv')
    filtered_codes_filename = 'ALL_CODES_filtered.csv'
    disch_full_filename = 'disch_full.csv'
    # split data by text
    #labeled_notes_filename = combine_notes_codes(disch_full_filename, filtered_codes_filename)
    #split_data()
    #split_data(is_full=False)
    #split_data_gzsl()

    #preprocess_icd9()
    if retrain_w2v:
        embed_words()
        save_data('cunyin/pretrained_model/w2v_mimic/disch_full.w2v', './data/mimic3/generated/disch_full.w2v')
    else:
        download_file('cunyin/pretrained_model/w2v_mimic/disch_full.w2v', './data/mimic3/generated/disch_full.w2v')
    #get_vectors(dataset_config)
    save_model(dataset_config)