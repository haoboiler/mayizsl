from cgi import test
import pandas as pd
import numpy as np
import re
from codes.wos.dataset_config import WOSDataConfig, WOSDataConfig_W2V
from codes.utils import download_file, save_data
from keras.preprocessing.text import Tokenizer
import torch
#from torchtext.data import get_tokenizer
from sklearn.model_selection import train_test_split
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

import os
import json
import numpy as np
import re

"""
WoS Reference: https://github.com/kk7nc/HDLTex
"""

FILE_DIR = '../data/wos/Data.csv'
total_len = []
np.random.seed(7)

english_stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll",
                     "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's",
                     'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs',
                     'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am',
                     'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does',
                     'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while',
                     'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during',
                     'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over',
                     'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all',
                     'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only',
                     'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't",
                     'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't",
                     'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't",
                     'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn',
                     "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won',
                     "won't", 'wouldn', "wouldn't"]


def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = string.strip().strip('"')
    # string = re.sub(r"[^A-Za-z0-9(),!?\.\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " ", string)
    string = re.sub(r"\.", " ", string)
    string = re.sub(r"\"", " ", string)
    string = re.sub(r"!", " ", string)
    string = re.sub(r"\(", " ", string)
    string = re.sub(r"\)", " ", string)
    string = re.sub(r"\?", " ", string)
    string = re.sub(r"\s{2,}", " ", string)
    # words = string.split(' ')
    # final_words = []
    # for word in words:
    #     if word.lower() in english_stopwords:
    #         pass
    #     else:
    #         final_words.append(word)
    # string = ' '.join(final_words)
    return string.strip().lower()


def text_cleaner(text):
    """
    cleaning spaces, html tags, etc
    parameters: (string) text input to clean
    return: (string) clean_text 
    """
    text = text.replace(".", "")
    text = text.replace("[", " ")
    text = text.replace(",", " ")
    text = text.replace("]", " ")
    text = text.replace("(", " ")
    text = text.replace(")", " ")
    text = text.replace("\"", "")
    text = text.replace("-", "")
    text = text.replace("=", "")
    rules = [
        {r'>\s+': u'>'},  # remove spaces after a tag opens or closes
        {r'\s+': u' '},  # replace consecutive spaces
        {r'\s*<br\s*/?>\s*': u'\n'},  # newline after a <br>
        {r'</(div)\s*>\s*': u'\n'},  # newline after </p> and </div> and <h1/>...
        {r'</(p|h\d)\s*>\s*': u'\n\n'},  # newline after </p> and </div> and <h1/>...
        {r'<head>.*<\s*(/head|body)[^>]*>': u''},  # remove <head> to </head>
        {r'<a\s+href="([^"]+)"[^>]*>.*</a>': r'\1'},  # show links instead of texts
        {r'[ \t]*<[^<]*?/?>': u''},  # remove remaining tags
        {r'^\s+': u''}  # remove spaces at the beginning
    ]
    for rule in rules:
        for (k, v) in rule.items():
            regex = re.compile(k)
            text = regex.sub(v, text)
        text = text.rstrip()
        text = text.strip()
    clean_text = text.lower()
    return clean_text


stats = {'Root': {'CS': 0, 'Medical': 0, 'Civil': 0, 'ECE': 0, 'biochemistry': 0, 'MAE': 0, 'Psychology': 0}, 
         'CS': {'Symbolic computation': 402, 'Computer vision': 432, 'Computer graphics': 412, 'Operating systems': 380, 'Machine learning': 398, 'Data structures': 392, 'network security': 445, 'Image processing': 415, 'Parallel computing': 443, 'Distributed computing': 403, 'Algorithm design': 379, 'Computer programming': 425, 'Relational databases': 377, 'Software engineering': 416, 'Bioinformatics': 365, 'Cryptography': 387, 'Structured Storage': 43},
         'Medical': {"Alzheimer's Disease": 368, "Parkinson's Disease": 298, 'Sprains and Strains': 142, 'Cancer': 359, 'Sports Injuries': 365, 'Senior Health': 118, 'Multiple Sclerosis': 253, 'Hepatitis C': 288, 'Weight Loss': 327, 'Low Testosterone': 305, 'Fungal Infection': 372, 'Diabetes': 353, 'Parenting': 343, 'Birth Control': 335, 'Heart Disease': 291, 'Allergies': 357, 'Menopause': 371, 'Emergency Contraception': 291, 'Skin Care': 339, 'Myelofibrosis': 198, 'Hypothyroidism': 315, 'Headache': 341, 'Overactive Bladder': 340, 'Irritable Bowel Syndrome': 336, 'Polycythemia Vera': 148, 'Atrial Fibrillation': 294, 'Smoking Cessation': 257, 'Lymphoma': 267, 'Asthma': 317, 'Bipolar Disorder': 260, "Crohn's Disease": 198, 'Idiopathic Pulmonary Fibrosis': 246, 'Mental Health': 222, 'Dementia': 237, 'Rheumatoid Arthritis': 188, 'Osteoporosis': 320, 'Medicare': 255, 'Psoriatic Arthritis': 202, 'Addiction': 309, 'Atopic Dermatitis': 262, 'Digestive Health': 95, 'Healthy Sleep': 129, 'Anxiety': 262, 'Psoriasis': 128, 'Ankylosing Spondylitis': 321, "Children's Health": 350, 'Stress Management': 361, 'HIV/AIDS': 358, 'Depression': 130, 'Migraine': 178, 'Osteoarthritis': 305, 'Hereditary Angioedema': 182, 'Kidney Health': 90, 'Autism': 309, 'Schizophrenia': 38, 'Outdoor Health': 2},
         'Civil': {'Green Building': 418, 'Water Pollution': 446, 'Smart Material': 363, 'Ambient Intelligence': 410, 'Construction Management': 412, 'Suspension Bridge': 395, 'Geotextile': 419, 'Stealth Technology': 148, 'Solar Energy': 384, 'Remote Sensing': 384, 'Rainwater Harvesting': 441, 'Transparent Concrete': 3, 'Highway Network System': 4, 'Nano Concrete': 7, 'Bamboo as a Building Material': 2, 'Underwater Windmill': 1},
         'ECE': {'Electric motor': 372, 'Satellite radio': 148, 'Digital control': 426, 'Microcontroller': 413, 'Electrical network': 392, 'Electrical generator': 240, 'Electricity': 447, 'Operational amplifier': 419, 'Analog signal processing': 407, 'State space representation': 344, 'Signal-flow graph': 274, 'Electrical circuits': 375, 'Lorentz force law': 44, 'System identification': 417, 'PID controller': 429, 'Voltage law': 54, 'Control engineering': 276, 'Single-phase electric power': 6},
         'biochemistry': {'Molecular biology': 746, 'Enzymology': 576, 'Southern blotting': 510, 'Northern blotting': 699, 'Human Metabolism': 622, 'Polymerase chain reaction': 750, 'Immunology': 652, 'Genetics': 566, 'Cell biology': 552, 'DNA/RNA sequencing': 14},
         'MAE': {'Fluid mechanics': 386, 'Hydraulics': 402, 'computer-aided design': 371, 'Manufacturing engineering': 346, 'Machine design': 420, 'Thermodynamics': 361, 'Materials Engineering': 289, 'Strength of materials': 335, 'Internal combustion engine': 387},
         'Psychology': {'Prenatal development': 389, 'Attention': 416, 'Eating disorders': 387, 'Borderline personality disorder': 376, 'Prosocial behavior': 388, 'False memories': 362, 'Problem-solving': 360, 'Prejudice': 389, 'Antisocial personality disorder': 368, 'Nonverbal communication': 394, 'Leadership': 350, 'Child abuse': 404, 'Gender roles': 395, 'Depression': 380, 'Social cognition': 397, 'Seasonal affective disorder': 365, 'Person perception': 391, 'Media violence': 296, 'Schizophrenia': 335}}
# 1-10 ['Electric motor', 'Satellite radio', 'Single-phase electric power']
# 4-4 ['Water Pollution', 'Bamboo as a Building Material', 'Underwater Windmill']
# 6-1 ['Cell biology', 'DNA/RNA sequencing']
# 4-10 ['Smart Material', 'Transparent Concrete', 'Nano Concrete']
# 1-9 ['Electrical generator', 'Analog signal processing']
# 4-1 ['Geotextile', 'Highway Network System']
# 5-7 ['Atrial Fibrillation', 'Depression']
# 5-10 ['Bipolar Disorder', 'Schizophrenia']
# 5-17 ['Digestive Health', 'Outdoor Health']


def build_label_matrix(df, tokenizer):
    label_df = pd.DataFrame()
    label_id_list = []
    label_parent_list = []
    label_name_list = []
    # deal with label parent
    for index, row in df.iterrows():
        root = row["Y1"] + 134
        root_name = row['Domain']
        if root not in label_id_list:
            label_id_list.append(root)
            label_name_list.append(root_name)
            label_parent_list.append(np.nan)
    label_df['id'] = label_id_list
    label_df['label'] = label_name_list
    label_df['parent'] = label_parent_list


    for index, row in df.iterrows():
        root = row["Y1"] + 134
        root_name = row["Domain"]
        label_1 = row["Y"]
        label_name = row["area"]
        if label_1 not in list(label_df["id"]):
            label_df.loc[len(label_df.index)] = [label_1, label_name, root]
            label_id_list.append(label_id_list)
    label_df.sort_values(by="id", inplace=True)

    label_df.to_csv('../data/wos/label.csv', index=False)
    return label_df
        


def split_dataset(df, label_df, by_files=False):
    '''
    Split dataset to train, test eval
    by files: 通过文件区分labeled与unlabeled数据
    '''
    if by_files:
        train_data_store_place = '../data/wos/11k_X.txt'
        with open(train_data_store_place) as f:
            sentence_list = f.readlines()
            sentence_list = [a[:-1] for a in sentence_list]
    
        for index, row in df.iterrows():
            text = row['Abstract']
            if text in sentence_list:
                df.loc[index, 'labeled'] = 1
            else:
                df.loc[index, 'labeled'] = 0

        labeled_df = df[df['labeled'] == 1][["label", "area", "data"]]
        unlabeled_df = df[df['labeled'] == 0][["label", "area", "data"]]
    
    else:
        # random choosed some unfortunate labels as zero-shot label, and random choose lot of text in labeled data as unlabeled data
        labeled_keys = []
        unlabeled_keys = []
        for i in range(134, 141):
            little_keys = list(label_df[label_df['parent']==i]['id'])
            little_keys = list(set(little_keys))
            labeled_keys += little_keys[:-2]
            unlabeled_keys += little_keys[-2:]
        print(labeled_keys)
        print(unlabeled_keys)        
                    
        
        # random sample choosed label data 
        df["label"] = df["Y"]
        df["data"] = df["clean_text"]
        labeled_df = pd.DataFrame()
        unlabeled_df = df[df["label"].isin(unlabeled_keys)]
        for label in labeled_keys:
            tmp_df = df[df["label"] == label]
            tmp_label_df, tmp_unlabel_df = train_test_split(tmp_df, test_size=0.8, random_state=0)
            labeled_df = labeled_df.append(tmp_label_df)
            unlabeled_df = unlabeled_df.append(tmp_unlabel_df)
        labeled_df = labeled_df[["label", "area", "data"]]
        unlabeled_df = unlabeled_df[["label", "area", "data"]]
        print(len(labeled_df))
        print(len(unlabeled_df))
        

    
    train_df, test_df = train_test_split(labeled_df, test_size=0.2, random_state=0)
    eval_df, test_df = train_test_split(test_df, test_size=0.5, random_state=0)
    unlabeled_train_df, unlabeled_test_df = train_test_split(unlabeled_df, test_size=0.1, random_state=0)
    test_df = pd.concat([test_df, unlabeled_test_df])
    
    train_df.to_csv('../data/wos/train.csv')
    eval_df.to_csv('../data/wos/eval.csv')
    test_df.to_csv('../data/wos/test.csv')
    unlabeled_train_df.to_csv('../data/wos/unlabeled.csv')


def clean_data(data, tokenizer):
    sequences = tokenizer.texts_to_sequences(data)
    rebuild_content = []
    for i in range(len(sequences)):
        text = data[i]
        sequence = sequences[i]
        rebuild_text = tokenizer.sequences_to_texts([sequence])
        rebuild_content.append(rebuild_text[0])
    assert len(rebuild_content) == len(data)
    return rebuild_content



def get_data_from_wos_origin():
    statics_df = pd.DataFrame()
    df = pd.read_csv(FILE_DIR)
    with open('../data/wos/Dictionary.txt') as f:
        word_dict = f.readlines()
        word_dict = [a[:-1] for a in word_dict]
        
    content = ['.'.join(clean_str(x).split('.')[:2]) for x in df['Abstract']]
    domain = [clean_str(x) for x in df['Domain']]
    area = [clean_str(x) for x in df['area']]
    tokenizer = Tokenizer(num_words=50000)
    tokenizer.fit_on_texts(content + domain + area)
    
    df['clean_text'] = clean_data(content, tokenizer)
    df['Domain'] = clean_data(domain, tokenizer)
    df['area'] = clean_data(area, tokenizer)
    print(df["Y"].value_counts())
    df.to_csv('../data/wos/clean_data.csv')

    lengths = []
    for text in df['clean_text']:
        lengths.append(len(text.split(' ')))
    df['lengths'] = lengths
    print(df['lengths'].describe())
    
    label_df = build_label_matrix(df, tokenizer)
    split_dataset(df, label_df)


def get_data_from_meta():
    statics_df = pd.DataFrame()
    df = pd.read_csv(FILE_DIR)
    with open('../data/wos/Dictionary.txt') as f:
        word_dict = f.readlines()
        word_dict = [a[:-1] for a in word_dict]
    lengths = []
    data = []
    label_check = {}
    for index, row in df.iterrows():
        sample_label = [row['Domain'].rstrip().lstrip(), row['area'].rstrip().lstrip()]
        code = str(row['Y1']) + '-' + str(row['Y2'])

        if code in label_check.keys():
            if sample_label[1] not in label_check[code]:
                label_check[code].append(sample_label[1])
        else:
            label_check[code] = [sample_label[1]]
       # print(label_check)

        for i in label_check[code]:
            if stats[sample_label[0]][i] > stats[sample_label[0]][sample_label[1]]:
                sample_label[1] = i
                break
        print(sample_label)
        # if sample_label[1] == 'Underwater Windmill':
        #     sample_label[1] = 'Water Pollution'
        # if sample_label[1] == 'Bamboo as a Building Material':
        #     sample_label[1] = 'Water Pollution'
        # if sample_label[1] == 'Nano Concrete':
        #     sample_label[1] = 'Smart Material'
        # if sample_label[1] == 'Highway Network System':
        #     sample_label[1] = 'Geotextile'
        # if sample_label[1] == 'Transparent Concrete':
        #     sample_label[1] = 'Smart Material'
        # if sample_label[1] == 'Outdoor Health':
        #     sample_label[1] = ''
        doc = row['Abstract']
        doc = clean_str(doc)
        sample_text = doc
        unknow_word_count = 0
        doc_words = doc.split(' ')
        # for word in doc_words:
        #     if word not in word_dict:
        #         print(word)
        #         unknow_word_count += 1
        # print(len(doc_words))
        # print(unknow_word_count)
        lengths.append(len(doc_words))
        total_len.append(len(sample_text))
        data.append({'doc_token': sample_text, 'doc_label': sample_label, 'doc_topic': [], 'doc_keyword': [], 'unk count': unknow_word_count})
    print(label_check)
    c = 0
    for i in label_check.keys():
        if len(label_check[i]) > 1:
            print(i, label_check[i])
            c += len(label_check[i]) - 1
    print(c)
    print(len(label_check.keys()))
    f = open('wos_total.json', 'w')
    for line in data:
        line = json.dumps(line)
        f.write(line + '\n')
    f.close()
    
    # 统计长度
    statics_df['lengths'] = lengths
    print(statics_df['lengths'].describe())


def split_train_dev_test():
    f = open('wos_total.json', 'r')
    data = f.readlines()
    f.close()
    id = [i for i in range(46985)]
    np_data = np.array(data)
    np.random.shuffle(id)
    np_data = np_data[id]
    train, test = train_test_split(np_data, test_size=0.2, random_state=0)
    train, val = train_test_split(train, test_size=0.2, random_state=0)
    train = list(train)
    val = list(val)
    test = list(test)
    f = open('wos_train.json', 'w')
    f.writelines(train)
    f.close()
    f = open('wos_test.json', 'w')
    f.writelines(test)
    f.close()
    f = open('wos_val.json', 'w')
    f.writelines(val)
    f.close()

    print(len(train), len(val), len(test))
    return


def get_hierarchy():
    f = open('wos_total.json', 'r')
    data = f.readlines()
    f.close()
    label_hierarchy = {}
    label_hierarchy['Root'] = []
    for line in data:
        line = line.rstrip('\n')
        line = json.loads(line)
        line = line['doc_label']
        if line[0] in label_hierarchy:
            if line[1] not in label_hierarchy[line[0]]:
                label_hierarchy[line[0]].append(line[1])
        else:
            label_hierarchy['Root'].append(line[0])
            label_hierarchy[line[0]] = [line[1]]
    f = open('wos.taxnomy', 'w')
    for i in label_hierarchy.keys():
        line = [i]
        line.extend(label_hierarchy[i])
        line = '\t'.join(line) + '\n'
        f.write(line)
    f.close()


def get_all_word_dict():
    # 统计所有出现的词
    label_df = pd.read_csv('../data/wos/label.csv')
    text = list(label_df['label'])
    print('first', label_df['label'])
    for file_name in ['train', 'test', 'eval', 'unlabeled']:
        df = pd.read_csv('../data/wos/%s.csv' % file_name)
        text += list(df['data']) + list(df['da_data'])
        
    total_word = []
    for sentence in text:
        sentence = sentence.split(' ')
        for word in sentence:
            total_word.append(word)
        total_word = list(set(total_word))
    print(total_word, len(total_word))
    return total_word, label_df['label']


def build_glove_w2v():
    # 根据出现的词build词表（用于word2vec）
    config = WOSDataConfig_W2V()
    # Convert
    input_file = '../data/wos/glove.6B.200d.txt'
    output_file = 'gensim_glove.6B.200d.txt'
    glove2word2vec(input_file, output_file)

    # Test Glove model
    model = KeyedVectors.load_word2vec_format(output_file, binary=False)
    
    strings = ['unk', '<unk>', 'UNK', '<UNK>', 'sos', '<bos>', 'BOS', '<BOS>', 'eos', '<eos>', 'EOS', '<EOS>', 'pad', '<pad>', 'PAD', '<PAD>', 'mask' ]
    for string in strings:
        try:
            model.key_to_index[string]
            print(string, model.key_to_index[string])
            print(model[string])
        except:
            print('no', string)
    # 
    big_vocab_dict = model.key_to_index
    big_weight = torch.FloatTensor(model.vectors)
    word_list, label_list = get_all_word_dict()
    word_list.append('unk')
    word_list.append('bos')
    word_list.append('eos')
    word_list.append('sos')
    word_list.append('pad')
    word_list.append('mask')
    word_list = list(set(word_list))
    choosed_word_dict = {}
    choosed_word_idx = []
    for word in word_list:
        if word not in choosed_word_dict.keys():
            if word not in big_vocab_dict.keys():
                print('both not word', word)
            else:
                choosed_word_idx.append(big_vocab_dict[word])
                choosed_word_dict[word] = len(choosed_word_dict)
    idx = torch.tensor(choosed_word_idx)
    final_weight = big_weight[idx]
    final_word_dict = choosed_word_dict
    # print(final_weight.shape)
    # print(final_weight.shape)
    # print(len(final_word_dict))
    for label in label_list:
        #print('label is', label)
        label = label.split(' ')
        for word in label:
            if word in final_word_dict.keys():
                print('word in', word, final_word_dict[word])
            else:
                print('word not in', word)

    np.save(config.w2v_vocab_path, final_word_dict)
    torch.save(final_weight, config.w2v_model_path)


if __name__ == '__main__':
    get_data_from_meta()
    get_hierarchy()
    split_train_dev_test()