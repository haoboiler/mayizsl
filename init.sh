tree
pip install aii_pypai -i https://pypi.antfin-inc.com/simple/ -U
pip install --upgrade transformers
pip install sklearn -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install nltk -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install synonyms -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install jieba -i https://pypi.tuna.tsinghua.edu.cn/simple
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install boto3 -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install kmeans-pytorch -i https://pypi.tuna.tsinghua.edu.cn/simple

mkdir ./data/pkgs

echo "pip list"
pip list
echo "ls /dev"
ls /dev