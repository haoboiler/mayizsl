from codes.trainflow import KESUBERTFlow


if __name__ == '__main__':
    download = True
    init = False
    dataset_dict = {'easy_label': True}

    for contras_p in [True, False]:
        for contras_t in [True, False]:
            for generate in [True, False]:
                for unlabel in [True, False]:
                    for lr in [0.0005, 0.001]:
                        model_dict = {'contras_p': contras_p,
                                      'contras_t': contras_t,
                                      'generate': generate,
                                      'unlabel': unlabel,
                                      'disperse_vq_type': 'Hard',
                                      'distil_vq_type': 'Fix_GCN',
                                      'vae_type': 'Fix_GCN',
                                      'lr': lr,
                                      # 'decoder_type': "GPT",
                                      'decoder_type': 'GPT_EASY',
                                      'encoder_type': 'Ernie',
                                      'encoder_p_type': 'Ernie_P',
                                      'start_unlabel_epoch': 3,
                                      'start_generate_epoch': 13,
                                      'batch_size': 48,
                                      'generate_size': 24,
                                      }
                        print('###########################################this task set is:', model_dict, dataset_dict)
                        flow = KESUBERTFlow(dataset_dict=dataset_dict, model_dict=model_dict, emb_type='bert', parallel=False, model='FixGCN',
                                            debug=False, init=init, local=False, download=download, easy_label=True)
                        flow.run()
                        download = False  # only need download in the first time
                        init = False
