import torch
import argparse
import sys
sys.path.append('/workspace/bin')
from codes.trainflow import KESUFlow

try:
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", default=-1, type=int)
    args = parser.parse_args()

    torch.cuda.set_device(args.local_rank)
except:
    print('use one card')

if __name__ == '__main__':
    download = True
    dataset_dict = {}
    model_dict = {'lr': 5e-5,
                  'max_epochs': 15
                    }       
    #for model in ['EASYSIM', 'AttnXML', 'TransICD', 'FZML']:
    for model in ['Bert_RL', 'ZeroGen', 'BERTClassifier']:
        flow = KESUFlow(dataset_dict=dataset_dict, model_dict=model_dict, emb_type='bert', parallel=False, model=model,
                            debug=False, init=False, local=False, download=download, last_label=True, decoder_gpt=True,
                            entailment=True, pre_idx=True)
        flow.run()
        download = False  # only need download in the first time
    
