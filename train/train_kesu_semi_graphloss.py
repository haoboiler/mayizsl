import torch
import argparse
from codes.trainflow import KESUFlow, AGFlow

try:
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", default=-1, type=int)
    args = parser.parse_args()

    torch.cuda.set_device(args.local_rank)
except:
    print('use one card')

if __name__ == '__main__':
    download = True
    init = False
    dataset_dict = {'easy_label': True}

    for contras_p in [True, False]:
        for contras_t in [True, False]:
            for generate in [True, False]:
                for unlabel in [True, False]:
                    for lr in [1e-3, 0.0005]:
                        
                        model_dict = {'contras_p': contras_p,
                                      'contras_t': contras_t,
                                      'generate': True,
                                      'unlabel': False,
                                      'lr': lr,
                                      'disperse_vq_type': 'DVQ',
                                      'distil_vq_type': 'Striaght',
                                      'classifier_type': 'EuclideanGraph',
                                      'discri_type': 'Linear',
                                      'graph_type': 'GAT',
                                      'decoder_type': 'TransformerDistance',
                                      'encoder_type': 'Transformer',
                                      'encoder_p_type': 'Transformer',
                                      'start_unlabel_epoch': 0,
                                      'start_generate_epoch': 0,
                                      'batch_size': 80,
                                      'generate_size': 32,
                                      'start_vq_epoch': 0,
                                      'easy_decoder': False,
                                      'use_memory': False,
                                      'num_layers': 2,
                                      'disper_num': 8,
                                      'use_w2v_weight': True
                                      }         
                        print('###########################################this task set is:', model_dict, dataset_dict)
                        emb_type = 'w2v'
                        flow = KESUFlow(dataset_dict=dataset_dict, model_dict=model_dict, emb_type=emb_type,
                                        parallel=True, model='Semi',
                                        debug=False, init=init, local=False, download=download, last_label=True)

                        flow.run()
                        download = False  # only need download in the first time
                        init = False
