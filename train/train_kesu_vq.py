from codes.trainflow import KESUFlow


if __name__ == '__main__':
    download = True
    init = True
    dataset_dict = {'easy_label': True}

    for contras_p in [True, False]:
        for contras_t in [True, False]:
            for generate in [True, False]:
                for unlabel in [True, False]:
                    for lr in [0.001, 0.0005]:
                        model_dict = {'contras_p': contras_p,
                                      'contras_t': contras_t,
                                      'generate': False,
                                      'unlabel': False,
                                      'disperse_vq_type': 'Hard',
                                      'distil_vq_type':'Hard',
                                      'vae_type': 'VQ_VAE_Idx',
                                      'lr': lr,
                                      'decoder_type': 'GPT',
                                       # 'decoder_type': 'GPT_Match',
                                      'encoder_type': 'Transformer',
                                      'encoder_p_type': 'Transformer_P',
                                      'start_unlabel_epoch': 0,
                                      'start_generate_epoch': 0,
                                      'batch_size': 200,
                                      'generate_size': 32,
                                      'pretrain': True,
                                      'pretrained_epochs': 40
                                      }
                        print('###########################################this task set is:', model_dict, dataset_dict)
                        flow = KESUFlow(dataset_dict=dataset_dict, model_dict=model_dict, emb_type='w2v', parallel=True, model='New_VQ',
                                        debug=False, init=init, local=False, download=download, last_label=True)
                        flow.run()
                        download = False  # only need download in the first time
                        init = False
