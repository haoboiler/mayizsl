import torch
import argparse
import sys
sys.path.append('/workspace/bin')
from codes.trainflow import KESUFlow

try:
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", default=-1, type=int)
    args = parser.parse_args()

    torch.cuda.set_device(args.local_rank)
except:
    print('use one card')

if __name__ == '__main__':
    print('start train and task')
    download = False
    init = False
    dataset_dict = {'easy_label': True}

    for contras_p in [True, False]:
        for contras_t in [True, False]:
            for generate in [True, False]:
                for unlabel in [True, False]:
                    for lr in [5e-5, 3e-5, 2e-5]:
                        
                        model_dict = {'contras_p': False,
                                      'contras_t': False,
                                      'ablation_generate_train': True,
                                      'use_new_self_training': False,
                                      'use_pseudo_label': True,
                                      'generate': True,
                                      'unlabel': True,
                                      'lr': lr,
                                      'disperse_vq_type': 'GS',
                                      'distil_vq_type': 'Fix',
                                      'graph_type': 'Straight',
                                      'decoder_type': 'GPT2',
                                      'encoder_type': 'Bert_Entail_CLS',
                                      'encoder_p_type': 'Bert_Pooler',
                                      'start_unlabel_epoch': 0,
                                      'start_generate_epoch': 0,
                                      'batch_size': 10,
                                      'generate_size': 10,
                                      'start_vq_epoch': 0,
                                      'easy_decoder': True,
                                      'use_memory': False,
                                      'num_layers': 4,
                                      'disper_num': 8,
                                      'encoder_output_size': 768,
                                      'decoder_input_size': 32
                                      }         
                        print('###########################################this task set is:', model_dict, dataset_dict)
                        emb_type = 'bert'
                        flow = KESUFlow(dataset_dict=dataset_dict, model_dict=model_dict, emb_type=emb_type,
                                        parallel=True, model='EntailmentSemiDis',
                                        debug=False, init=init, local=False, download=download, last_label=True, decoder_gpt=True,
                                        entailment=True, pre_idx=True)

                        flow.run()
                        download = False  # only need download in the first time
                        init = False
