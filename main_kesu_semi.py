# -*- coding: utf-8 -*-
from pypai.job.python_job_builder import PythonJobBuilder
from pypai.utils.km_conf_utils import KubemakerRole, ResourceSpec
from pypai.conf import KMConf, ExecConf


def main():
    image = 'reg.docker.alibaba-inc.com/aii/aistudio:532-20210531110459'
    master = ExecConf(cpu=10, memory=1024 * 64, gpu_num=8, num=1, gpu_type="v100", disk_m=1024 * 200)
    km_conf = KMConf(
        cluster='stl',  # stl, alipay_et15 （两个公共池子）
        image=image
    )
    commands = 'source init.sh && python ./train/download_wos.py --emb_type w2v && python -m torch.distributed.launch --nproc_per_node 8 ./train/train_kesu_semi_graphloss.py'
    gpu_builder = PythonJobBuilder(
        source_root='.',
        command=commands,
        main_file='./train/train_kesu_semi_graphloss.py',
        master=master,
        km_conf=km_conf,
        k8s_app_name='riskmining',
        k8s_priority='high'
    )
    gpu_builder.run()


if __name__ == '__main__':
    main()
