# from model.new_model.train import OneModelTrainer, OneModelTrainerParallel
from model.new_model.train_new import (
    OneModelTrainer_SemiVAE,
    OneModelTrainerParallel_SemiVAE,
    OneModelTrainer_SemiVAELabel,
    OneModelTrainerParallel_SemiVAELabel,
    )
from model.new_model.train_simple import (
    OneModelTrainer_SimpleFixLabel,
    OneModelTrainerParallel_SimpleFixLabel
)
from model.new_model.train_entailment_no_dis import (
    OneModelTrainer_Entailment,
    OneModelTrainer_EntailmentImbalance,
    OneModelTrainerParallel_Entailment,
    OneModelTrainerParallel_EntailmentImbalance,
    OneModelTrainerParallel_EntailmentImbalanceSemi
)

from model.new_model.train_entailmen_dis import (
    OneModelTrainer_Entailment_Dis,
    OneModelTrainer_EntailmentImbalance_Dis,
    OneModelTrainerParallel_Entailment_Dis,
    OneModelTrainerParallel_EntailmentImbalance_Dis,
    OneModelTrainerParallel_EntailmentImbalanceSemi_Dis
)
from model.gpt2model.train import OneModelTrainer_GPT2, OneModelTrainerParallel_GPT2
from model.bertmodel.train import OneModelTrainer_BERT, OneModelTrainerParallel_Bert
from model.transicd.train import OneModelTrainer_TransICD, OneModelTrainerParallel_TransICD
from model.fzml.train import OneModelTrainer_MLZS, OneModelTrainerParallel_MLZS
from model.attentionxml.train import OneModelTrainer_AttnXML, OneModelTrainerParallel_AttnXML
from model.easy_sim.train import OneModelTrainer_SIM, OneModelTrainerParallel_SIM
from model.zerogen.train import OneModelTrainer_ZeroGen, OneModelTrainerParallel_ZeroGen
from model.bert_rl.train import OneModelTrainer_RL
# from model.discrete_vae.train import OneModelTrainer_DisVAE, OneModelTrainerParallel_DisVAE


ModelTrainer = {'w2v': {
    # 'New': OneModelTrainer,
    # 'New_VQ': OneModelTrainer_VQVAE,
    # 'GAN_VQ': OneModelTrainer_VQVAE_GAN,
    'TransICD': OneModelTrainer_TransICD,
    # 'Fix': OneModelTrainer_VQVAESplit,
    'FZML': OneModelTrainer_MLZS,
    'AttnXML': OneModelTrainer_AttnXML,
    'EASYSIM': OneModelTrainer_SIM,
    # 'DisVAE': OneModelTrainer_DisVAE,
    'Semi': OneModelTrainer_SemiVAE,
    'SemiLabel': OneModelTrainer_SemiVAELabel,
    'SimpleLabel': OneModelTrainer_SimpleFixLabel,
    'GPT2Classifier': OneModelTrainer_GPT2,
    'BERTClassifier': OneModelTrainer_BERT
    },
    'bert': {
    # 'DisVAE': OneModelTrainer_DisVAE,
    'Semi': OneModelTrainer_SemiVAE,
    'SemiLabel': OneModelTrainer_SemiVAELabel,
    'SimpleLabel': OneModelTrainer_SimpleFixLabel,
    'GPT2Classifier': OneModelTrainer_GPT2,
    'BERTClassifier': OneModelTrainer_BERT,
    'Entailment': OneModelTrainer_Entailment,
    'Entailment2': OneModelTrainer_EntailmentImbalance,
    'ZeroGen': OneModelTrainer_ZeroGen,
    'Bert_RL': OneModelTrainer_RL
    }
}

ModelTrainerParallel = {'w2v': {
    # 'New': OneModelTrainerParallel,
    'TransICD': OneModelTrainerParallel_TransICD,
    # 'New_VQ': OneModelTrainerParallel_VQVAE,
    # 'GAN_VQ': OneModelTrainerParallel_VQVAE_GAN,
    # 'Fix': OneModelTrainerParallel_VQVAESplit,
    'FZML': OneModelTrainerParallel_MLZS,
    'AttnXML': OneModelTrainerParallel_AttnXML,
    'EASYSIM': OneModelTrainerParallel_SIM,
    # 'DisVAE': OneModelTrainerParallel_DisVAE,
    'Semi': OneModelTrainerParallel_SemiVAE,
    'SemiLabel': OneModelTrainerParallel_SemiVAELabel,
    'SimpleLabel': OneModelTrainerParallel_SimpleFixLabel,
    'GPT2Classifier': OneModelTrainerParallel_GPT2,
    'BERTClassifier': OneModelTrainerParallel_Bert,
    },
    'bert': {
    # 'DisVAE': OneModelTrainerParallel_DisVAE,
    'Semi': OneModelTrainerParallel_SemiVAE,
    'SemiLabel': OneModelTrainerParallel_SemiVAELabel,
    'SimpleLabel': OneModelTrainerParallel_SimpleFixLabel,
    'GPT2Classifier': OneModelTrainerParallel_GPT2,
    'BERTClassifier': OneModelTrainerParallel_Bert,
    'Entailment': OneModelTrainerParallel_Entailment,
    'Entailment2': OneModelTrainerParallel_EntailmentImbalance_Dis,
    'EntailmentSemi': OneModelTrainerParallel_EntailmentImbalanceSemi,
    'EntailmentSemiDis': OneModelTrainerParallel_EntailmentImbalanceSemi_Dis,
    'ZeroGen': OneModelTrainerParallel_ZeroGen
    }
}